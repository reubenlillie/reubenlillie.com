/**
 * @file Contains global author data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global author data module
 * @module _data/author
 */
export default [
  {
    name: 'Apprentice Artist',
    organization: {
      name: 'Sugar Creek Opera',
      website: 'http://sugarcreekopera.com/'
    },
    years: {
      start: 2008,
      end: 2010
    }
  },
  {
    name: 'Apprentice Artist',
    organization: {
      name: 'The Santa Fe Opera',
      website: 'https://www.santafeopera.org/'
    },
    years: [2013]
  },
  {
    name: 'Apprentice Artist',
    organization: {
      name: 'Des Moines Metro Opera',
      website: 'https://desmoinesmetroopera.org/'
    },
    years: [2014, 2016]
  }
]
