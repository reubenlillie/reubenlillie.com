/**
 * @file Defines the 404 page layout
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Acts as front matter data in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in JavaScript templates in Eleventy}
 */
export var data = {
  eleventyExcludeFromCollections: true,
  layout: 'layouts/page',
  permalink: '404.html',
  title: 'Oops! Not Found!'
}

/**
 * Defines markup for the 404 page
 * @return {string} HTML
 * @see {@link https://www.11ty.dev/docs/quicktips/not-found/ 404 pages in Eleventy}
 */
export function render() {
  return `<!-- ./404.11ty.js -->
Sorry, the content you are looking for cannot be found at that address.

Do you want to try one of these options instead?

* Go to the [home page](/)
* [Contact me](/contact/)`
}
