/**
 * @file Defines a shortcode for displaying a copyright notice
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Defines markup for a copyright notice
 * @module _includes/shortcodes/copyright-notice
 * @since 1.0.0
 * @param {Object} Global data from `./_data/copyright.js`
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.copyrightNotice(data)}`
 */
export default ({holder, license, year}) => 
`<!-- ./_includes/shortcodes/copyright-notice.js -->
<span id="copyright_year">&copy; ${year}</span>
<span id="copyright_holder">by ${holder}.</span>
<span id="copyright_license"><abbr title="${license.abbr}: ${license.name}">${license.abbr}</abbr>.</span>`
