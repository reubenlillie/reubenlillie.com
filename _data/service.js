/**
 * @file Contains global data for academic appointments
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global data module for academic and community service
 * @module _data/service
 * @since 2.0.0
 */
export default [
  {
    start: new Date(2023, 9, 18),
    role: 'Board of Trustees',
    institution: {
      name: 'McCormick Theological Seminary',
      address: {
        street: '5416 S. Cornell Ave, 5th Floor',
        city: 'Chicago',
        state: {
          abbr: 'IL',
          name: 'Illinois'
        },
        zip: 60915
      },
      website: 'https://mccormick.edu/'
    },
  },
  {
    start: new Date(2022, 9, 20),
    role: 'Alumn/æ Council',
    institution: {
      name: 'McCormick Theological Seminary',
      address: {
        street: '5416 S. Cornell Ave, 5th Floor',
        city: 'Chicago',
        state: {
          abbr: 'IL',
          name: 'Illinois'
        },
        zip: 60915
      },
      website: 'https://mccormick.edu/'
    },
  }
]
