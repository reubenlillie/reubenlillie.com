/**
 * @file Defines a filter to sort an array by a specified propertyate
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Sort an array by a specified property
 * @param {Object[]} An array of Objects with a `date` property
 * @param {string} A property name
 * @param {boolean} Whether to reverse the sorted array
 * @return {string} Formatted string
 */
export default (arr, prop, reverse = false) => {
  var sorted = arr.sort((a, b) => a[prop] - b[prop])
  return reverse
    ? sorted.reverse()
    : sorted
}
