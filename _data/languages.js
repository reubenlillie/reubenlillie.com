/**
 * @file Contains global languages data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global languages data module
 * @module _data/languages
 * @since 1.0.0
 */
export default {
  ancient: [
    {
      name: 'Biblical Hebrew',
      proficiency: 'intermediate'
    },
    {
      name: 'Koine Greek',
      proficiency: 'intermediate'
    },
    {
      name: 'Latin',
      proficiency: 'beginner'
    }
  ],
  modern: [
    {
      name: 'French',
      proficiency: 'intermediate'
    },
    {
      name: 'German',
      proficiency: 'intermediate'
    },
    {
      name: 'Italian',
      proficiency: 'intermediate'
    },
    {
      name: 'Spanish',
      proficiency: 'intermediate'
    }
  ],
  programming: [
    {
      name: 'HTML (Living Standard)',
      proficiency: 'advanced'
    },
    {
      name: 'CSS3',
      proficiency: 'advanced'
    },
    {
      name: 'JavaScript (ECMAScript 6+)',
      proficiency: 'advanced'
    },
    {
      name: 'Lua',
      proficiency: 'intermediate'
    },
    {
      name: 'PHP',
      proficiency: 'intermediate'
    }
  ],
  singing: [
    {
      name: 'Czech'
    },
    {
      name: 'Danish'
    },
    {
      name: 'Korean'
    },
    {
      name: 'Norwegian'
    },
    {
      name: 'Russian'
    },
    {
      name: 'Swedish'
    }
  ]
}
