/**
 * @file Contains global data for academic appointments
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global data module for academic appointments
 * @module _data/appointments
 * @since 1.0.0
 */
export default [
  {
    start: new Date(2016, 7, 23),
    role: 'Adjunct Professor',
    institution: {
      name: 'Olivet Nazarene University',
      address: {
        street: 'One University Avenue',
        city: 'Bourbonnais',
        state: {
          abbr: 'IL',
          name: 'Illinois'
        },
        zip: 60914
      },
      departments: [
        {
          name: 'School of Music',
          coursework: 'Voice'
        },
        {
          name: 'School of Theology and Christian Ministry',
          coursework: 'Theology and Philosophy'
        },
        {
          name: 'Martin D. Walker School of Engineering',
          coursework: 'Ethical Web Development',
          syllabus: 'comp-496'
        }
      ],
      website: 'https://olivet.edu/'
    },
  }
]  
