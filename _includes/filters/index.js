/**
 * @file Imports modules and configures them as filtes with Eleventy
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import fileExists from './file-exists.js'
import fileToString from './file-to-string.js'
import fromCloudinary from './from-cloudinary.js'
import formatTitle from './format-title.js'
import formatUrl from './format-url.js'
import fullName from './full-name.js'
import initialLetter from './initial-letter.js'
import isDate from './is-date.js'
import minifyCSS from './minify-css.js'
import minifyJS from './minify-js.js'
import sortBy from './sort-by.js'

/**
 * Configure imported filters with Eleventy
 * @module _includes/filters
 * @param {Object} eleventyConfig Eleventy’s Config object
 * @return {void}
 * @since 1.0.0
 * @see {@link https://www.11ty.dev/docs/config/ Configuring Eleventy}
 * @see {@link https://www.11ty.dev/docs/filters Filters in Eleventy}
 */
export default eleventyConfig => {
  eleventyConfig.addFilter('fileExists', fileExists)
  eleventyConfig.addFilter('fileToString', fileToString)
  eleventyConfig.addFilter('fromCloudinary', fromCloudinary)
  eleventyConfig.addFilter('fullName', fullName)
  eleventyConfig.addFilter('formatTitle', formatTitle)
  eleventyConfig.addFilter('formatUrl', formatUrl)
  eleventyConfig.addFilter('initialLetter', initialLetter)
  eleventyConfig.addFilter('isDate', isDate)
  eleventyConfig.addFilter('minifyCSS', minifyCSS)
  eleventyConfig.addFilter('minifyJS', minifyJS)
  eleventyConfig.addFilter('sortBy', sortBy)

  return
}
