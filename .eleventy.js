/**
 * @file Configures Eleventy options and helper methods
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Eleventy’s Syntax Highlighting plugin
 * @see {@link https://www.11ty.dev/docs/plugins/syntaxhighlight/ Eleventy documentation}
 */
import syntaxHighlight from '@11ty/eleventy-plugin-syntaxhighlight'

/**
 * Import the following modules in
 *
 * - Filters (modify content on input)
 * - Shortcodes (for reusable content)
 * - Transforms (modify a template’s output)
 *
 * Storing these modules in separate directories,
 * rather than all in this file,
 * helps keep the codebase organized—at least that’s the idea.
 */
import includes from './_includes/index.js'

/**
 * Eleventy’s configuration module
 * @module .eleventy
 * @param {Object} eleventyConfig Eleventy’s Config API
 * @return {Object} {} Eleventy’s optional Config object
 * @see {@link https://www.11ty.dev/docs/config/}
 */
export default function (eleventyConfig) {
  // Pass Eleventy’s Conif object to the includes directory
  includes(eleventyConfig)

  /**
   * Copies static assets to the output directory
   * @see {@link https://www.11ty.dev/docs/copy/ Eleventy docs}
   */
  eleventyConfig.addPassthroughCopy('css')
  eleventyConfig.addPassthroughCopy('favicons')
  eleventyConfig.addPassthroughCopy('fonts')
  eleventyConfig.addPassthroughCopy('img')
  eleventyConfig.addPassthroughCopy('site.webmanifest')
  eleventyConfig.addPassthroughCopy('pdf')

  /**
   * Adds syntax highlighting for code snippets
   * @see {@link https://github.com/11ty/eleventy-plugin-syntaxhighlight Plugin repo on GitHub}
   */
  eleventyConfig.addPlugin(syntaxHighlight, {
    templateFormats: ['md']
  })
}
