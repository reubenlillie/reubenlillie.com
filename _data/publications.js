/**
 * @file Contains global publications data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global publications data module
 * @module _data/publications
 * @since 1.0.0
 */
export default [
  {
    type: 'magazine',
    date: new Date(2024, 11, 2),
    title: 'Luke 1:68–79',
    publication: {
      name: 'A Plain Account'
    },
    url: 'https://www.aplainaccount.org/post/luke-1-68-79-1'
  },
  {
    type: 'website',
    date: new Date(2020, 6, 8),
    title: 'Responding to Voddie Baucham’s “Ethnic Gnosticism”',
    publication: {
      page: 'Resources on Justice and Race',
      owner: 'Nazarene Theological Seminary'
    },
    url: 'https://www.academia.edu/43571279/Responding_to_Voddie_Baucham_s_Ethnic_Gnosticism_'
  },
  {
    type: 'magazine',
    date: new Date(2020, 6, 12),
    title: 'Time for a Break',
    publication: {
      name: 'Standard'
    },
    url: 'https://www.academia.edu/44042843/Time_for_a_Break'
  },
  {
    type: 'journal',
    date: new Date(2020, 6, 27),
    coauthors: [
      {
        first: 'Charles',
        middle: 'L',
        last: 'Perabeau'
      }
    ],
    title: 'Redefining Districts: Fulfilling a New Model for Administrative Boundaries in the Church of the Nazarene',
    publication: {
      name: 'Didache',
      volume: 20,
      issue: 1
    },
    url: 'http://didache.nazarene.org/index.php/filedownload/didache-volumes/vol-20/1273-didache-v20n1-02-redefining-districts-lillie-perabeau'
  },
  {
    type: 'magazine',
    date: new Date(2021, 2, 16),
    title: 'Better Line Breaks for Long URLs',
    publication: {
      name: 'CSS-Tricks'
    },
    url: 'https://css-tricks.com/better-line-breaks-for-long-urls/'
  }
]
