/**
 * @file Defines a shortcode to load a favicon for the user’s platform
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Load the appropriate favicon
 * @module _includes/shortcode/favicon
 * @since 1.0.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.favicon(data)}`
 * @see {@link https://realfavicongenerator.net/ Favicon Generator}
 */
export default ({author: {fullName}, colors, pkg: {version}}) => {
  return `<!-- ./shortcodes/favicon.js -->
<link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png?v=${version}" media="screen">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png?v=${version}" media="screen">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png?v=${version}" media="screen">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg?v=${version}" color="#101820" media="screen">
    <link rel="shortcut icon" href="/favicons/favicon.ico?v=${version}">
    <meta name="apple-mobile-web-app-title" content="${fullName}">
    <meta name="application-name" content="${fullName}">
    <meta name="msapplication-TileColor" content="#ce7639">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml?v=${version}">
    <meta name="theme-color" content="${colors.primary}">`
}
