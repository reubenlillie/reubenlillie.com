/**
 * @file Defines a filter for verifying `Date` objects
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Checks if a parameter is a JavaScript `Date` object
 * @param {*} Any parameter
 * @return {boolean} If the parameter is a `Date` object
 */
export default date => 
  Reflect.apply(Object.prototype.toString, '[object Date]', date)
