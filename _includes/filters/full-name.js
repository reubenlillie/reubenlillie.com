/**
 * @file Defines a filter for formatting a person’s full name
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Formats a person’s full name
 * @module _includes/filters/full-name
 * @since 1.0.0
 * @param {Object} fullName A name object
 * @return {string} The formatted name
 */
export default ({first, middle, last}) => {
  // Is the person using a middle initial?
  middle = middle.length === 1
    // Then add the period
    ? `${middle}.`
    // Then display the full middle name, if any
    : middle

  return `${first} ${middle} ${last}`
}
