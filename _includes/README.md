# Lloyd

## Summary

_Lloyd is a simpler Eleventy theme_

Named for my paternal grandfather, Lloyd Gorton Lillie (1927–2000), this [Eleventy](https://11ty.dev/) (11ty) theme is built with only [JavaScript templates](https://www.11ty.dev/docs/languages/javascript/) (`*.11ty.js`).

Links to corresponding [11ty documentation](https://11ty.dev/docs/)

* Assets (CSS, fonts, images, multimedia, and other [passthrough copy](https://www.11ty.dev/docs/copy/))
* [Filters](https://www.11ty.dev/docs/filters/)
* [Layouts](https://www.11ty.dev/docs/layouts/)
* [Shortcodes](https://www.11ty.dev/docs/shortcodes/)
* [Transforms](https://www.11ty.dev/docs/config/#transforms)
