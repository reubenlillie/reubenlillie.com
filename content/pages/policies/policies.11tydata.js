/**
 * @file Contains metadata common to all policy pages, to reduce repetition
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * A directory data module for site policies
 * @module content/policies
 * @since 1.0.0
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in 11ty}
 */
export default {
  date: 'Last Modified',
  showDate: true,
  tags: 'policies'
}
