/**
 * @file Defines a shortcode for displaying a page’s date
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Defines markup for a page date
 * @module _includes/shortcodes/page-date
 * @since 1.0.0
 * @param {Object} date From Eleventy’s `data.page` object
 * @return {string} HTML
 * @see {@link https://www.11ty.dev/docs/dates/ Content dates in Eleventy}
 * @example 
 * // In an Eleventy template
 * `${this.pageDate(data.page.date)}`
 */
export default ({page: {date}, site: {locale}}, options) => {
  /** 
   * Merge options 
   * @type {Object}
   */
  options = {...locale.dateOptions, ...options}

  // Set UTC date
  date = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getTimezoneOffset() / 60, 0, 0, 0))

  return `<!-- ./_includes/shortcodes/page-date.js -->
<time>${date.toLocaleDateString(locale, options)}</time>`
}
