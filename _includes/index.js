/**
 * @file Imports modules and configures them with Eleventy
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import filters from './filters/index.js'
import shortcodes from './shortcodes/index.js'
import transforms from './transforms/index.js'

/**
 * Configures imported modules with Eleventy
 * @module _includes/
 * @param {Object} eleventyConfig Eleventy’s Config object
 * @return {void}
 * @since 1.0.0
 * @see {@link https://www.11ty.dev/docs/config/ Configuring Eleventy}
 * @see {@link https://www.11ty.dev/docs/filters Filters in Eleventy}
 * @see {@link https://www.11ty.dev/docs/shortcodes Shortcodes in Eleventy}
 * @see {@link https://www.11ty.dev/docs/config/#transforms Transforms in Eleventy}
 */
export default eleventyConfig => {
  filters(eleventyConfig)
  shortcodes(eleventyConfig)
  transforms(eleventyConfig)

  return
}
