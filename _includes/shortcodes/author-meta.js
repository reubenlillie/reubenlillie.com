/**
 * @file Defines a shortcode for author metadata
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in Eleventy}
 */

/**
 * Markup for author metadata
 * @module _includes/shortcodes/author-meta
 * @since 1.0.0
 * @param {Object} data Eleventy’s data object
 * @return {string} HTML template literal
 * @example `${this.authorMeta(data)}`
 */
export default ({author: {name: {fullName}}, copyright, title}) =>
`<!--./_includes/shortcodes/author-meta.js-->
<meta name="author"
  content="${fullName
    ? fullName
    : copyright.holder
      ? copyright.holder
      : title}">`
