/**
 * @file Contains global software data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global software data module
 * @module _data/software
 * @since 1.0.0
 * @since 1.5.0 ⛏️ Add Mine Shaft
 */
export default [
  {
    name: '🍦.11ty.js',
    description: 'A vanilla JavaScript starter kit for the static site generator <a href=\"https://11ty.dev/\"> Eleventy</a>',
    url: 'https://eleventy-dot-js-blog.netlify.app/'
  },
  {
    name: 'GratuiTip™',
    description: 'A state-based online tip calculator',
    url: 'https://gratuitip.netlify.com/'
  },
  {
    name: 'The Lectionary.app',
    description: 'A progressive web app for readings from the Daily Office Lectionary',
    url: 'https://lectionary.app/'
  },
  {
    name: 'Leveraging the ‘J’ in Jamstack',
    description: 'An online slideshow <a href=\'/presentations/\'>presentation</a> about JavaScript and the Jamstack, itself written in vanilla JavaScript',
    url: 'https://gitlab.com/reubenlillie/j-in-jamstack/'
  },
  {
    name: '<em>Manual of the Church of the Nazarene</em>',
    description: 'The electronic edition of the official agreed-upon statement of faith, practice, and polity for the International Church of the Nazarene; built with the custom WordPress plugin <a href=\'https://github.com/reubenlillie/manualmaker\'>ManualMaker</a>; undergoing development in 42 languages',
    url: 'https://manual.nazarene.org/'
  },
  {
    name: '⛏️ Mine Shaft',
    description: 'An accessible color scheme for <a href=\"https://neovim.io/\">Neovim</a> written in Lua',
    url: 'https://github.com/reubenlillie/mine-shaft'
  }
]
