/**
 * @file Contains global data for formatting dates
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global date formatting options module
 * @module _data/dateOptions
 * @since 1.0.0
 */
export default {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric'
}
