/**
 * @file Contains metadata common to all pages, to reduce repetition
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Directory data module
 * @module content/pages
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in 11ty}
 */
export default {
  layout: 'layouts/content',
  tags: 'pages',
  eleventyComputed: {
    permalink: data => `/${data.page.fileSlug}/index.html`,
  }
}
