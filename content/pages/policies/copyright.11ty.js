/**
 * @file Defines the home Copyright Notice layout
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @since 1.0.0
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in JavaScript templates in Eleventy}
 */
export var data = {
  description: 'Copyright and licensing policies for all content and source code of my personal website–written in plain language.',
  shortTitle: 'Copyright',
  templateEngineOverride: '11ty.js,md',
  title: 'Copyright Notice',
  weight: 1
}

/**
 * Defines markup for the Copyright Notice page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render() {
  return `<!--content/pages/policies/copyright.11ty.js-->

I value creativity, and I want you to feel free to improve upon my work.

## Copyrighted Content

Unless otherwise noted, content on this site is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license][cc-by-sa].

You are free to copy and redistribute the material in any medium or format. You are also free to remix, transform, and build upon the material for any purpose, even commercially. I will not revoke these terms as long as you follow the terms of this license.

You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests I endorse you or your use.

If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For further permissions, please [contact me][contact].

## Source Code License

The source code for this site is licensed under the [MIT License][mit]. A copy of the license is available in the source code repository on [GitLab][mit].

Reuben L. Lillie

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[mit]: https://gitlab.com/reubenlillie/reubenlillie.com/blob/master/LICENSE/`
}
