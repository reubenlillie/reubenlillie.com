/**
 * @file Defines a filter to check if a file exists
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import {existsSync} from 'fs'

/**
 * Check if a file exists
 * @module _includes/filters/file-exists
 * @return {string} The file’s contents
 * @see {@link `fs.existsSync(path) in Node.js`}
 * @example `${this.fileExists('_includes/assets/css/internal.css')}`
 */
export default path => 
  typeof path === 'string'
    ? existsSync(`${path}`)
    : console.error(`Cannot read path ${path} in ./_includes/filters/fileExists()`)
