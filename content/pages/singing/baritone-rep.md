---
title: Archive of Baritone Repertoire
tags: singing
layout: layouts/content
eleventyExcludeFromCollections: true
description: Here’s a sample of my stage and concert work as a baritone from 2007 to 2019 before I transitioned to singing tenor in 2020.
---

This page highlights some of my [stage](#stage) and [concert](#concert) work from 2007 to 2019 when I performed as a baritone.

In those early years, I explored roles as low as Sarastro (my first complete role) and Leporello from Mozart’s <cite>The Magic Flute</cite> and <cite>Don Giovanni</cite> and as high as Malatesta from Donizetti’s <cite>Don Pasquale</cite> and Figaro from Rossini’s <cite>The Barber of Seville</cite>.

I was humbled to share six summers in some of the finest apprentice programs in the United States: Sugar Creek Opera (2007–’10), the Santa Fe Opera (2013), and Des Moines Metro Opera (2014, ’16).

In competition, I had the honor of winning the Chicago Chapter <abbr title="NATS: National Association of Teachers of Singing">NATS</abbr> in 2013 and being named a Regional Finalist in the Metropolitan Opera National Council Auditions in 2013 and 2016.

I also had the joy of presenting the Chicago premiere of the orchestrated version of Ralph Vaughan Williams’s <cite>Songs of Travel</cite> in 2012 at Symphony Center’s Orchestra Hall.

For non-vocal health reasons, I took a hiatus from singing in the summer and fall of 2017 to undergo multiple surgeries. As is often the case, I hadn’t realized how poorly I felt until after healing. When I returned to singing later that year, I found practically everything easier than ever before—high, low, loud, soft, resonance, coloratura, intonation—you name it. Over the following two years, I began seriously considering changes in my repertoire.

This archive represents my introduction to opera and the wide range of vocal experiments my teachers and coaches encouraged me to pursue before I finally transitioned to [tenor](/rep/) in 2020.

<section>
  <header>
    <h2>Former Baritone Stage Repertoire <a id="stage">#</a></h2>
  </header>
  <p>Here’s a sample of opera and musical theater roles I performed, covered, or studied as a baritone.</p>
  <p><em>Arranged alphabetically by composer</em></p>
  <h3>Mark Adamo</h3>
  <ul>
    <li><cite>Little Women</cite> (John Brooke)</li>
  </ul>
  <h3>Vincenzo Bellini</h3>
  <ul>
    <li><cite>I puritani</cite> (Sir Riccardo Forth)</li>
  </ul>
  <h3>Leonard Bernstein</h3>
  <ul>
    <li><cite>Candide</cite> (Pangloss, Voltaire, Governor, Maximilian, Martin,
    Frigate Captain)</li>
  </ul>
  <h3>Georges Bizet</h3>
  <ul>
    <li><cite>Carmen</cite> (Le Dancaïre, Escamillo, Morales)</li>
    <li><cite>Les pêcheurs de perles</cite> (Zurga)</li>
  </ul>
  <h3>Benjamin Britten</h3>
  <ul>
    <li><cite>A Midsummer Night’s Dream</cite> (Bottom, Demetrius)</li>
    <li><cite>Albert Herring</cite> (Sid)</li>
  </ul>
  <h3>Aaron Copeland</h3>
  <ul>
    <li><cite>The Tender Land</cite> (Top, Mr. Jenks)</li>
  </ul>
  <h3>John Corigliano</h3>
  <ul>
    <li><cite>The Ghosts of Versailles</cite> (Beaumarchais, Figaro)</li>
  </ul>
  <h3>Claude Debussy</h3>
  <ul>
    <li><cite>Pelléas et Mélisande</cite> (Pelléas)</li>
  </ul>
  <h3>Gaetano Donizetti</h3>
  <ul>
    <li><cite>Don Pasquale</cite> (Malatesta)</li>
    <li><cite>L’elisir d’amore</cite> (Belcore)</li>
    <li><cite>La fille du régiment</cite> (Sulpice)</li>
    <li><cite>Lucia di Lammermoor</cite> (Lord Enrico Ashton)</li>
  </ul>
  <h3>Philip Glass</h3>
  <ul>
    <li><cite>Galileo Galilei</cite> (Salviati)</li>
  </ul>
  <h3>Charles Gounod</h3>
  <ul>
    <li><cite>Faust</cite> (Valentin)</li>
    <li><cite>Roméo et Juliette</cite> (Mercutio)</li>
  </ul>
  <h3>Daron Aric Hagen</h3>
  <ul>
    <li><cite>Amelia</cite> (Paul)</li>
  </ul>
  <h3>Franz Léhar</h3>
  <ul>
    <li><cite>The Merry Widow</cite> (Danilo, Kromov, Zeta)</li>
  </ul>
  </ul>
  <ul>
  <h3>Frederick Loewe</h3>
  <ul>
    <li><cite>My Fair Lady</cite> (Henry Higgins, Freddy Eynsford-Hill)</li>
  </ul>
  <h3>Pietro Mascagni</h3>
  <ul>
    <li><cite>Cavalleria rusticana</cite> (Alfio)</li>
    <li><cite>Le maschere</cite> (Tartaglia)</li>
  </ul>
  <h3>Jules Massenet</h3>
  <ul>
    <li><cite>Hérodiade</cite> (Hérode)</li>
    <li><cite>Manon</cite> (Lescaut, Monsieur de Brétigny)</li>
    <li><cite>Thaïs</cite> (Athanaël)</li>
  </ul>
  <h3>Alan Menken</h3>
  <ul>
    <li><cite>Beauty and the Beast</cite> (Beast, Gaston)</li>
  </ul>
  <h3>Gian Carlo Menotti</h3>
  <ul>
    <li><cite>The Old Maid and the Thief</cite> (Ben)</li>
    <li><cite>The Telephone</cite> (Ben)</li>
  </ul>
  <h3>Darius Mihaud</h3>
  <ul>
    <li><cite>L’Enlèvement d’Europe</cite> (Pergamon)</li>
    <li><cite>L’abandon d’Ariane</cite> (Dionysos)</li>
    <li><cite>La Délivrance de Thésée</cite> (Hippolyte, Théramene)</li>
  </ul>
  <h3>Douglas Moore</h3>
  <ul>
    <li><cite>The Ballad of Baby Doe</cite> (Horace Tabor)</li>
  </ul>
  <h3>Theo Morrison</h3>
  <ul>
    <li><cite>Oscar</cite> (Jury Foreman)</li>
  </ul>
  <h3>Wolfgang Amadeus Mozart</h3>
  <ul>
    <li><cite>Così fan tutte</cite> (Guglielmo)</li>
    <li><cite>Die Zauberflöte</cite> (Sarastro, Papageno)</li>
    <li><cite>Don Giovanni</cite> (Don Giovanni, Leporello)</li>
    <li><cite>Le nozze di Figaro</cite> (Count Almaviva, Figaro)</li>
  </ul>
  <h3>Giacomo Puccini</h3>
  <ul>
    <li><cite>Edgar</cite> (Frank)</li>
    <li><cite>Gianni Schicchi</cite> (Gianni Schicchi)</li>
    <li><cite>La bohème</cite> (Marcello, Schaunard)</li>
    <li><cite>Madama Butterlfy</cite> (Sharpless)</li>
    <li><cite>Il tabarro</cite> (Michele)</li>
    <li><cite>Tosca</cite> (Scarpia)</li>
  </ul>
  <h3>Henry Purcell</h3>
  <ul>
    <li><cite>Dido and Aeneas</cite> (Aeneas)</li>
  </ul>
  <h3>Richard Rodgers</h3>
  <ul>
    <li><cite>Oklahoma!</cite> (Andrew Carnes, Curly McLain)</li>
  </ul>
  <h3>Gioachino Rossini</h3>
  <ul>
    <li><cite>Il barbiere di Siviglia</cite> (Figaro)</li>
    <li><cite>La cenerentola</cite> (Dandini)</li>
    <li><cite>L comte Ory</cite> (Raimbaud)</li>
    <li><cite>L’italiana in Algeri</cite> (Taddeo)</li>
  </ul>
  <h3>Claude-Michel Schönberg</h3>
  <ul>
    <li><cite>Les Misérables</cite> (Inspector Javert)</li>
  </ul>
  <h3>Stephen Sondheim</h3>
  <ul>
    <li><cite>A Little Night Music</cite> (Fredrik Egerman)</li>
    <li><cite>Sweeney Todd: The Demon Barber of Fleet Street</cite> (Sweeney
    Todd/Benjamin Barker)</li>
  </ul>
  <h3>Johann Strauss II</h3>
  <ul>
    <li><cite>Die Fledermaus</cite> (Gabriel von Eisenstein, Falke, Frank)</li>
  </ul>
  <h3>Richard Strauss</h3>
  <ul>
    <li><cite>Ariadne auf Naxos</cite> (Harlequin)</li>
  </ul>
  <h3>Pyotr Ilyich Tchaikovsky</h3>
  <ul>
    <li><cite>Iolanta</cite> (Robert)</li>
    <li><cite>Eugene Onegin</cite> (Eugene Onegin)</li>
  </ul>
  <h3>Ambroise Thomas</h3>
  <ul>
    <li><cite>Hamlet</cite> (Hamlet)</li>
  </ul>
  <h3>Giuseppi Verdi</h3>
  <ul>
    <li><cite>Ernani</cite> (Don Carlo)</li>
    <li><cite>Falstaff</cite> (Falstaff, Ford)</li>
    <li><cite>La traviata</cite> (Barone Duphol, Giorgio Germont)</li>
    <li><cite>Rigoletto</cite> (Rigoletto)</li>
  </ul>
  <h3>Richard Wagner</h3>
  <ul>
    <li><cite>Tannhäuser</cite> (Wolfram)</li>
  </ul>
  <h3>Frank Wildhorn</h3>
  <ul>
    <li><cite>Jekyll and Hyde</cite> (Dr. Henry Jekyll/Edward Hyde)</li>
  </ul>
  <h3>Meredith Willson</h3>
  <ul>
    <li><cite>The Music Man</cite> (Prof. Harold Hill)</li>
  </ul>
</section>
<section>
  <header>
    <h2>Former Baritone Concert Repertoire <a id="concert">#</a></h2>
  </header>
  <p>Here’s a sample of songs, cycles, cantatas, oratorios, and other works for solo voice I performed as a baritone.</p>
  <p><cite>Arranged alphabetically by composer</cite></p>
  <h3>Johann Sebastian Bach</h3>
  <ul>
    <li><cite>Magnificat</cite>, BWV 243a</li>
    <li><cite>Mass in B Minor</cite>, BWV 232</li>
  </ul>
  <h3>Samuel Barber</h3>
  <ul>
    <li><cite>Four Songs</cite>, Op. 13</li>
    <li><cite>Three Songs</cite>, Op. 10</li>
  </ul>
  <h3>Ludwig van Beethoven</h3>
  <ul>
    <li><cite>An die ferne Geliebte</cite>, Op. 98</li>
    <li>“Der Kuss”, Op. 128</li>
    <li>“Die Liebe”, Op. 52</li>
    <li>“Zärtliche Liebe” (or “Ich liebe dich”), WoO 123</li>
  </ul>
  <h3>Hector Berlioz</h3>
  <ul>
    <li><cite>L’enfance du Christ</cite>, Op. 25</li>
    <li><cite>Les nuits d’été</cite>, Op. 7</li>
  </ul>
  <h3>Antonio Caldara</h3>
  <ul>
    <li>“Sebben, crudele”</li>
  </ul>
  <h3>Giacomo Carissimi</h3>
  <ul>
    <li>“Vittoria, mio core!”</li>
  </ul>
  <h3>Ernest Chausson</h3>
  <ul>
    <li>“Le Charme”, Op. 2</li>
  </ul>
  <h3>Edwin T. Childs</h3>
  <ul>
    <li><cite>A Charles Wesley Trilogy</cite></li>
  </ul>
  <h3>Antonín Dvořák</h3>
  <ul>
    <li>“Já jsem ten rytíř z pohádkyi”, Op. 3, no. 3</li>
  </ul>
  <h3>Petr Eben</h3>
  <ul>
    <li>“Za gorum, za vodum” from <cite>Písně z Těšínska</cite></li>
  </ul>
  <h3>Gabriel Fauré</h3>
  <ul>
    <li><cite>Requiem</cite>, Op. 48</li>
  </ul>
  <h3>Carlisle Floyd</h3>
  <ul>
    <li><cite>Pilgrimage</cite></li>
  </ul>
  <h3>Niels Gade</h3>
  <ul>
    <li><cite>Tre Digte af Hans Christian Andersen</cite></li>
  </ul>
  <h3>Tommaso Giordani</h3>
  <ul>
    <li>“Caro mio ben”</li>
  </ul>
  <h3>Pavel Haas</h3>
  <ul>
    <li><cite>Čtyři písně na slova čínské poezi</cite></li>
  </ul>
  <h3>George Fredric Handel</h3>
  <ul>
    <li><cite>Cuopre tal volta il cielo</cite> , HWV 98</li>
    <li><cite>Messiah</cite>, HWV 56</li>
  </ul>
  <h3>Joseph Haydn</h3>
  <ul>
    <li><cite>Die Jahreszeiten</cite>, Hob. XXI:3</li>
    <li><cite>Die Schöpfung</cite>, Hob. XXI:2</li>
  </ul>
  <h3>Moses Hogan</h3>
  <ul>
    <li>“Deep River”</li>
    <li>“Somebody’s Knockin’ at Yo’ Door”</li>
    <li>“Sometimes I Feel Like a Motherless Child”</li>
  </ul>
  <h3>Arthur Honegger</h3>
  <header>
  </ul>
  <ul>
  <h3>Frederick Keel</h3>
  <ul>
    <li><cite>Four Salt-Water Ballads</cite></li>
    <li><cite>Three Salt-Water Ballads</cite></li>
  </ul>
  <h3>Ruggero Leoncavallo</h3>
  <ul>
    <li>“Mattinata”</li>
  </ul>
  <h3>Gustav Mahler</h3>
  <ul>
    <li><cite>Lieder eines fahrenden Gesellen</cite></li>
  </ul>
  <h3>Felix Mendelssohn</h3>
  <ul>
    <li><cite>Elijah</cite>, Op. 70, MWV A 25</li>
  </ul>
  <h3>Émile Paladilhe</h3>
  <ul>
    <li>“Psyché”</li>
  </ul>
  <h3>Francis Poulenc</h3>
  <ul>
    <li><cite>Quatre poèmes de Guillaume Apollinaire</cite>, FP 58</li>
  </ul>
  <h3>Roger Quilter</h3>
  <ul>
    <li><cite>Five Shakespeare Songs</cite></li>
    <li>“Love’s Philosophy”</li>
  </ul>
  <h3>Giaochino Rossini</h3>
  <ul>
    <li>“Musique Anodine” from <cite>Péchés de vieillesse</cite>, Vol. XIII,
    Nos. II and VI</li>
    <li><cite>Petite messe solennelle</cite></li>
  </ul>
  <h3>Jakub Jan Ryba</h3>
  <ul>
    <li><cite>Česká mše vánoční</cite>, HWV 98</li>
  </ul>
  <h3>John Charles Sacco</h3>
  <ul>
    <li>“Brother Will, Brother John”</li>
  </ul>
  <h3>Franz Schubert</h3>
  <ul>
    <li>“Der Erlkönig”, Op. 1, D 328</li>
    <li>“Die Fischer”, Op. 5, No. 3, D 225</li>
    <li>“Die Forelle”, Op. 32, D 550</li>
    <li>“Morgenlied”, Op. 4, No. 2, D 685</li>
    <li>“Ständchen”, D 889</li>
    <li>“Ständchen”, D 957</li>
    <li>“Der Wanderer”, Op. 4, No. 1, D 489</li>
    <li>“Wandrers Nachtlied”, Op. 4, No. 3, D 224</li>
    <li>“Der Zwerg”, Op. 22, No. 1, D 771</li>
  </ul>
  <h3>Robert Schumann</h3>
  <ul>
    <li><cite>Dichterliebe</cite>, Op. 48</li>
    <li><cite>Myrthen</cite>, Op. 25</li>
    <li><cite>Sechs Gedichte aus dem Liederbuch eines Malers</cite>, Op.
    36</li>
  </ul>
  <h3>Christian Sinding</h3>
  <ul>
    <li><cite>Jeg bærer den Hat, som jeg vil</cite></li>
  </ul>
  <h3>Louis Spohr</h3>
  <ul>
    <li><cite>Sechs deutsche Lieder für eine Singstimme, Klarinette und
    Klavier</cite>, Op. 103</li>
  </ul>
  <h3>Wilhelm Stenhammar</h3>
  <ul>
    <li><cite>Visor och stämnigar</cite>, Op. 26</li>
  </ul>
  <h3>Giuseppi Torelli</h3>
  <ul>
    <li>“Tu lo sai”</li>
  </ul>
  <h3>Ralph Vaughan Williams</h3>
  <ul>
    <li><cite>Five Mystical Songs</cite></li>
    <li><cite>Songs of Travel</cite></li>
    <li>“Silent Noon” from <cite>The House of Life</cite></li>
  </ul>
  <h3>William Walton</h3>
  <ul>
    <li><cite>Balshazzar’s Feast</cite></li>
  </ul>
  <h3>Peter Warlock</h3>
  <ul>
    <li>“Walking in the Woods”</li>
  </ul>
  <h3>Ovid Young</h3>
  <ul>
    <li>“I Lay My Sins on Jesus”</li>
  </ul>
</section>
