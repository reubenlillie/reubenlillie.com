/**
 * @file Defines the chained template for course syllabi
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  description: 'Here are some syllabi from university courses I have designed and taught.',
  tags: 'teaching',
  title: 'Syllabi',
  weight: 1
}

/**
 * Define markup for a syllabus listing
 * @param {Object} syllabus A syllabus data object
 * @return {string} HTML
 */
function syllabusCard({
  courseNumber, 
  institution, 
  school, 
  department, 
  title, 
  url
}) {
  return `<!-- syllabus() in ./content/pages/teaching/syllabi/index.11ty.js -->
<li>
  ${courseNumber ? `${courseNumber}:` : ''}
  ${title
    ? url ? `“<a href="${url}">${title}</a>.”` : `${title}.”`
    : ''}
  ${institution ? `${institution},` : ''}
  ${school ? `${school},` : ''}
  ${department ? `${department}.` : ''}
</li>`
}

/**
 * Defines markup for the syllabi listings page
 * @method
 * @name render()
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render({syllabi}) {
  return `<!--content/pages/teaching/syllabi/index.11ty.js-->
<p>Here are some syllabi from university courses I have created and taught.</p>
<p><em>Arranged by course number</em></p>
<ul class="hanging-indent no-list-style">
  ${syllabi.map(syllabus => syllabusCard(syllabus)).join('\n')}
</ul>`
}
