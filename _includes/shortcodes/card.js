/**
 * @file Defines a shortcode for displaying an information card
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Defines markup for an information card
 * @module _includes/shortcodes/card
 * @since 1.0.0
 * @param {Object} data Card data with a `description`, `name`, and `url`
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.copyrightNotice(data)}`
 */
export default ({description, name, url}) =>
`<!-- ./includes/shortcodes/card.js -->
<article>
  ${name ? `<h2>${name}</h2>` : ''}
  ${url ? `<p><a href="${url}">${url}</a></p>` : ''}
  ${description ? `<p>${description}</p>` : ''}
</article>`
