/**
 * @file Defines the home page layout
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in JavaScript templates in Eleventy}
 */
export var data = {
  eleventyComputed: {
    title: data => `Hi, I’m ${data.author.name.first}.`,
    shortTitle: data => `${data.author.name.fullName}`,
    image: {
      src: 'headshot.jpg',
      alt: data => `${data.author.name.first}’s headshot`
    },
  },
  layout: 'layouts/base',
  tags: 'headerNav',
  weight: 1
}

/**
 * Defines markup for the home page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export async function render(data) {
  /**
   * Destructure a portion of `data`
   * @type {Object} 
   */
  var {
    collections: {activities}, 
    image, 
    page, 
    site: {tagline}, 
    title
  } = data

  /**
   * Markup for my headshot 
   * @type {Promise<string>} 
   */
  var headshot = await this.headshot({
    src: image.src, 
    alt: image.alt,
    width: 300,
    height: 300,
  })

  return `<!-- ./index.11ty.js -->
${headshot}
<div class="wrapper">
  <h1 class="no-margin">${title}</h1>
  <p>${tagline}</p>
  <div class="wrapper">
    ${this.nav(activities, page)}
  </div>
</div>`
}
