/**
 * @file Defines a shortcode for the `<title>` markup
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in Eleventy}
 */

/**
 * Defines markup for the `<title>` in the HTML `head`
 * @module _includes/shortcodes/title-tag
 * @since 1.0.0
 * @param {Object} data Eleventy’s data object
 * @return {string} HTML template literal
 * @example 
 * // In an Eleventy template
 * `${this.titleTag(data)}`
 */
export default ({author: {name: {fullName}}, page: {fileSlug}, title}) =>
`<!--_includes/shortcodes/title-tag.js-->
<title>
  ${fileSlug
    ? `${fullName}’s ${title}`
    : title
  }
</title>`
