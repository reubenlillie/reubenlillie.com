/**
 * @file Defines the chained template for journal articles
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  description: 'Here’s a sample of my published journal articles.',
  shortTitle: 'Articles',
  tags: 'writing',
  templateEngineOverride: '11ty.js,md',
  title: 'Articles',
  weight: 1
}

/**
 * Defines markup for the articles listing page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render({publications}) {
  publications = this.sortBy(publications, 'date', true)
  return  `<!--content/pages/writing/articles.11ty.js-->

Here’s a sample of my published journal articles as well as some popular commissions.

You can find full text copies, abstacts, and other information at [Olivet Digital Commons][digital-commons] and [Academia.edu][academia].

_Arranged by date_

${publications.map(article => this.authorDateReference(article)).join('\n')}

[academia]: https://olivet.academia.edu/ReubenLillie
[digital-commons]: https://digitalcommons.olivet.edu/do/search/?q=author_lname%3A%22Lillie%22%20AND%20author_fname%3A%22Reuben%22&start=0&context=1036391&sort=date_desc`
}
