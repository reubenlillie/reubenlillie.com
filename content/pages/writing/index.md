---
title: Writing
tags: activities
weight: 2
description: Like what you’ve read so far? I’ve got more where that came from. Wish I’d write about something else? I just might if you ask me.
---

My main research interests lie at the intersection of social epistemology and ethics. Lately, I’ve been studying intellectual virtues, epistemic injustice, and the epistemology of disagreement.

I’m currently writing on intellectual humility and love, through a grant from the Craighton T. and Linda G. Hippenhammer Faculty Scholarship at Olivet Nazarene University.

You can sample some of my academic [articles](/articles/) and [papers](/papers/) on this site, on [PhilPapers][philPapers], at [Olivet Digital Commons][digital-commons], and [Academia.edu][academia].

I’m also active in the open-source software community, where I advocate for ethical web development practices. Olivet’s Walker School of Engineering invited me to create a [course](/comp-496/) on the subject.

You can sample some of my [projects](/coding-projects/) on this site as well as on [GitLab][gitlab] and [GitHub][github].

[academia]: https://olivet.academia.edu/ReubenLillie
[digital-commons]: https://digitalcommons.olivet.edu/do/search/?q=author_lname%3A%22Lillie%22%20AND%20author_fname%3A%22Reuben%22&start=0&context=1036391&sort=date_desc
[github]: https://github.com/reubenlillie/
[gitlab]: https://gitlab.com/reubenlillie/
[philPapers]: https://philpeople.org/profiles/reuben-l-lillie
