/**
 * @file Layout stylesheet
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/*
 * Custom properties specific to layout
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties Using CSS custom properties (variables) on MDN}
 */
:root {
  --paragraph-width: 66ch;
}

/* Universal box sizing */
*, 
  *:before, 
  *:after {
    box-sizing: border-box;
  }

  /* Print layouts do not need a body height */
  @media screen {
    body {
      min-height: calc(100vh - 2em);
    }
  }

  main {
    width: var(--paragraph-width, 68ch);
  }

  .has-background {
    background-size: cover;
    background-repeat: no-repeat;
    margin: auto;
  }

  /**
   * Prince does not support background blend mode
   * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/background-blend-mode MDN Contributors. 2021. “background-blend-mode.” MDN. January 9.}
   */
@media screen {
  .has-background {
    background-blend-mode: darken;
  }
}

/* Grid */

/** 
 * Prince does not support CSS grid 
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout MDN Contributors. 2020. “CSS Grid Layout.” MDN. December 30.}
 */
@media screen {
  .grid,
  .has-background{
    display: grid;
  }

  body {
    grid-template-columns: 1fr minmax(300px, var(--paragraph-width, 68ch)) 1fr;
    grid-template-rows: auto 1fr auto;
  }

  body > * { 
    max-width: calc(100vw - 1em);
  }

  #site_header {
    grid-column: 1 / span all;
  }

  #main {
    grid-row: 2;
    grid-column: 2;
  }

  #site_footer {
    grid-row: 3;
    grid-column: 1 / span all;
    grid-template-columns: repeat(auto-fit, minmax(20em, 1fr));
  }

  #site_footer nav ul,
  /* Used to separate columns in curriculum vitae */
  p.flex,
  .gap {
    gap: 1em;
  }
}


/* Flexbox */
.flex {
  display: flex;
}

nav[aria-label="activities"] ul,
.flex-wrap {
  flex-wrap: wrap;
}

.align-items-center {
  align-items: center;
}

@media screen and (min-width: 30em) {
  #site_footer nav {
    display: flex;
    justify-self: end;
  }
}

#site_footer nav ul,
nav ul.flex,
.flex-column {
  flex-direction: column;
}

nav[aria-label="social media nav"] ul.flex {
    flex-direction: row;
}

@media screen and (min-width: 20em) {
  nav ul.flex,
  #site_footer nav ul {
    flex-direction: row;
    flex-wrap: wrap;
  }

  nav[aria-label="activities"] ul {
    justify-content: space-evenly;
  }
}

@media screen and (min-width: 41.125em) {
  nav[aria-label="activities"] li {
    flex: 1;
  }
}

#site_header nav ul,
.space-between {
  justify-content: space-between;
}

.space-around {
  justify-content: space-around;
}

.justify-end {
  justify-content: end;
}

nav ul.flex {
  width: 100%;
}

@media (min-width: 30em) {
  /* Float */
  .float {
    clear: both;
  }

  .float-left {
    float: left;  
  }
}

/* Margin */
@media screen and (min-width: 21em) {
  .margin {
    margin: 1em;
  }

  main {
    max-width: calc(100vw - 2em);
  }

  .margin-inline-end {
    margin-right: 1em;
    margin-inline-end: 1em;
  }

  .margin-block-end {
    margin-bottom: 1em;
    margin-block-end: 1em;
  }
}

.no-margin-block {
  margin-top: 0;
  margin-bottom: 0;
}

/* Padding */
.no-padding-inline-start {
  padding-left: 0;
}

.padding-inline-end {
  padding-right: 1em;
}

th:not(:first-of-type),
td:not(:first-of-type) {
  padding-left: 1em;
}

@media screen {
  nav[aria-label="activities"] li,
  nav + nav li {
    padding-left: 1em;
    padding-right: 1em;
    padding-inline: 1em;
  }
}

/**
 * Prince does not support block and inline dimensions 
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Logical_Properties/Basic_concepts#block_and_inline_dimensions MDN Contributors. 2021. “Block and inline dimensions“ in “Basic concepts of Logical Properties and Values” MDN, January 11.}
 */
@media screen {
  .no-margin-block {
    margin-block-start: 0;
    margin-block-end: 0;
  }

  .no-padding-inline-start {
    padding-inline-start: 0;
  }

  .padding-inline-end {
    padding-inline-end: 1em;
  }

  th:not(:first-of-type),
  td:not(:first-of-type) {
    padding-left: 1em;
  }
}

.min-width {
  min-width: 1in;
}

/* Make widths more flexible on screens */
@media screen {
  .min-width {
    min-width: 9ch;
  }
}

ol,
ul {
  /* Prince uses margin instead of padding for lists */
  margin-left: inherit;
  padding-left: 1em;
}

/* 
 * Hide elements designed for screen readers 
 * @see {@link https://gomakethings.com/a11y-and-text-just-for-screen-readers/ Ferdinandi, Chris. 2016. “A11y and text just for screen readers.” Go Make Things. December 13.}
 */
.screen-reader {
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
}

.hidden {
  display: none;
  visibility: visible;
}

/* Hide on screen */
.hide-on-small {
  display: none;
}

@media (min-width: 40em) {
  .hide-on-small {
    display: inherit;
    visibility: visible;
  }
}

.show-in-print-only {
  display: none;
  visibility: hidden;
}

/**
 * Prince does not support shape outside
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/shape-outside MDN Contributors. 2022. “shape-outide.” MDN. September 26.}
 */
@media screen {
  .headshot {
    shape-outside: circle(50%);
  }
}

.headshot img {
  border-radius: 50%;
}

.home main {
  align-items: center;
  justify-content: center;
  text-align: center;
}

@media (min-width: 60em) {
  .home main {
    flex-direction: row !important;
  }
}

.social svg {
  height: 1em;
  width: auto;
  vertical-align: sub;
}
