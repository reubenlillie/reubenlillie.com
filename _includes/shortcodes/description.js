/**
 * @file Defines a shortcode for the description metadata
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in Eleventy}
 */

/**
 * Markup for description metadata to be placed in the HTML `head`
 * @module _includes/shortcodes/description
 * @since 1.0.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.description(data)}`
 */
export default ({description, pkg, site}) => 
`<!-- ./_includes/shortcodes/description.js -->
<meta name="description"
  content="${description
    ? description
    : site.description
      ? site.description
      : pkg.description}">`
