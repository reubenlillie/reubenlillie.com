/**
 * @file Defines a filter to capitalize an initial letter
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Capitalizes the initial letter of a string
 * @param {string} Unformatted word or string
 * @return {string} Formatted string
 */
export default word => `${word.charAt(0).toUpperCase()}${word.substring(1)}`
