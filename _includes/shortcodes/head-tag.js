/**
 * @file Defines a shortcode for the `<head>` markup
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

// Filters
import fileToString from '../filters/file-to-string.js'
import minifyJS from '../filters/minify-js.js'

// Shortcodes
import authorMeta from './author-meta.js'
import description from './description.js'
import externalCSS from './external-css.js'
import favicon from './favicon.js'
import internalCSS from './internal-css.js'
import socialMeta from './social-meta.js'
import titleTag from './title-tag.js'

/**
 * Defines markup for the HTML `<head>`
 * @module _includes/shortcodes/head-tag
 * @since 1.0.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.headTag(data)}`
 */
export default async data =>
`<!--_includes/shortcodes/head-tag.js-->
<head>
  ${titleTag(data)}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script async>
    ${await minifyJS(fileToString('js/font-loading.js'))}
  </script>
  <style>
    ${internalCSS(data)}
  </style>
  <link rel="preload" href="/fonts/courier-prime/CourierPrime-latin.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="manifest" href="/site.webmanifest?v=${data.pkg.version}" media="screen">
  ${favicon(data)}
  ${externalCSS(data)}
  ${authorMeta(data)}
  ${socialMeta(data)}
  ${description(data)}
</head>`
