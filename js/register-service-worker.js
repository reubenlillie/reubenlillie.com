/**
 * @file Register the app service worker
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @since 0.3.0
 * @see {@link https://gomakethings.com/writing-your-first-service-worker-with-vanilla-js/ “Writing Your First Service Worker in Vanilla JS” by Chris Ferdinandi}
 */ 

// Immediately invoked function expression
(function () {
  // Client supports service workers
  if(navigator && navigator.serviceWorker) {
    // Register the service worker in the project root
    navigator.serviceWorker.register('/service-worker.min.js');
  }
  return
}())
