import fileExists from '../filters/file-exists.js'
import fileToString from '../filters/file-to-string.js'

/**
 * Defines markup for a social media link
 * @param {object} account Social media account data
 */
export default ({name, user, url}) => {
  var lowerCase = name.toLowerCase()
  var icon = `img/icons/${lowerCase}.svg`
  return `<!--_includes/shortcodes/social-link.js-->
<li>
  <a class="social" href="${url}">${fileExists(icon)
    ? `<!--has icon-->
<span class="${lowerCase}">${fileToString(icon)}</span>
<span class="hide-on-small smaller no-line-break">${user}</span>`
    : `<!--no icon-->
  ${name}
  `}</a>
</li>`
}
