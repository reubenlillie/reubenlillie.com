/**
 * @file Contains global publications data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global publications data module
 * @module _data/publications
 * @since 1.0.0
 */
export default [
  {
    type: 'paper',
    coauthors: [
      {
        first: 'Charles',
        middle: 'L',
        last: 'Perabeau'
      }
    ],
    date: new Date(2025, 2, 15),
    title: 'Interdependence as Participation: An Ecclesiology of District Administration in the Church of the Nazarene',
    presentedAt: 'to be presented at the',
    sponsor: '60th annual meeting of the Wesleyan Theological Society',
    location: {
      name: 'George W. Truett Theological Seminary, Baylor University',
      city: 'Waco',
      state: 'TX'
    }
  },
  {
    type: 'paper',
    date: new Date(2025, 2, 14),
    title: 'Dirty Words in the Book of Job: Excavating a Poetic Vocabulary of Divine Participation',
    presentedAt: 'to be presented at the',
    sponsor: '60th annual meeting of the Wesleyan Theological Society',
    location: {
      name: 'George W. Truett Theological Seminary, Baylor University',
      city: 'Waco',
      state: 'TX'
    }
  },
]
