/**
 * @file Imports modules and configures them as transforms with Eleventy
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import minifyHTML from './minify-html.js'

/**
 * Configure imported transforms with Eleventy
 * @module _includes/transforms
 * @param {Object} eleventyConfig Eleventy’s Config object
 * @return {void}
 * @since 1.0.0
 * @see {@link https://www.11ty.dev/docs/config/ Configuring Eleventy}
 * @see {@link https://www.11ty.dev/docs/config/#transforms Transforms in Eleventy}
 */
export default eleventyConfig => {
  eleventyConfig.addTransform('minifyHTML', minifyHTML)

  return
}
