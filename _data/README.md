# [Using Data in Eleventy](https://www.11ty.dev/docs/data/) (11ty)

This directory contains [global data files](https://www.11ty.dev/docs/data-global/).

[site.js](https://gitlab.com/reubenlillie/reubenlillie.com/-/blob/master/src/data/site.js) contains metadata that is accessed by a number of files for configuring the site.
