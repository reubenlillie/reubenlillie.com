/**
 * @file Defines a filter to convert a file’s contents to a string
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Import the Node.js fs module for interacting with the file system
 * @see {@link https://nodejs.dev/learn/the-nodejs-fs-module The Node.js fs module}
 */
import {readFileSync} from 'fs'

/**
 * Converts a file’s contents to a string
 * @param {string} path The path of the file to convert (relative to root)
 * @return {string} The file’s contents
 * @example 
 * // In an Eleventy template
 * `${this.fileToString('includes/assets/css/inline.css')}`
 */
export default path => readFileSync(`${path}`).toString()
