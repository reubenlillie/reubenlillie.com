/**
 * @file Imports modules and configures them as shortcodes with Eleventy
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import authorDateReference from './author-date-reference.js'
import authorMeta from './author-meta.js'
import backgroundImage from './background-image.js'
import blockquote from './blockquote.js'
import card from './card.js'
import copyrightNotice from './copyright-notice.js'
import corpus from './corpus.js'
import description from './description.js'
import editThisPage from './edit-this-page.js'
import externalCSS from './external-css.js'
import favicon from './favicon.js'
import headTag from './head-tag.js'
import headshot from './headshot.js'
import internalCSS from './internal-css.js'
import listCoauthors from './list-coauthors.js'
import madeWith from './made-with.js'
import nav from './nav.js'
import pageDate from './page-date.js'
import pdfLink from './pdf-link.js'
import siteFooter from './site-footer.js'
import siteHeader from './site-header.js'
import socialLink from './social-link.js'
import socialMeta from './social-meta.js'
import titleTag from './title-tag.js'

/**
 * Configure imported shortcodes with Eleventy
 * @module _includes/shortcodes
 * @param {Object} eleventyConfig Eleventy’s Config object
 * @return {void}
 * @since 1.0.0
 * @see {@link https://www.11ty.dev/docs/config/ Configuring Eleventy}
 * @see {@link https://www.11ty.dev/docs/shortcodes Shortcodes in Eleventy}
 */
export default eleventyConfig => {
  eleventyConfig.addShortcode('authorDateReference', authorDateReference)
  eleventyConfig.addShortcode('authorMeta', authorMeta)
  eleventyConfig.addShortcode('backgroundImage', backgroundImage)
  eleventyConfig.addShortcode('blockquote', blockquote)
  eleventyConfig.addShortcode('card', card)
  eleventyConfig.addShortcode('copyrightNotice', copyrightNotice)
  eleventyConfig.addShortcode('corpus', corpus)
  eleventyConfig.addShortcode('description', description)
  eleventyConfig.addShortcode('editThisPage', editThisPage)
  eleventyConfig.addShortcode('externalCSS', externalCSS)
  eleventyConfig.addShortcode('favicon', favicon)
  eleventyConfig.addShortcode('headTag', headTag)
  eleventyConfig.addShortcode('headshot', headshot)
  eleventyConfig.addShortcode('internalCSS', internalCSS)
  eleventyConfig.addShortcode('listCoauthors', listCoauthors)
  eleventyConfig.addShortcode('madeWith', madeWith)
  eleventyConfig.addShortcode('nav', nav)
  eleventyConfig.addShortcode('pageDate', pageDate)
  eleventyConfig.addShortcode('pdfLink', pdfLink)
  eleventyConfig.addShortcode('siteFooter', siteFooter)
  eleventyConfig.addShortcode('siteHeader', siteHeader)
  eleventyConfig.addShortcode('socialLink', socialLink)
  eleventyConfig.addShortcode('socialMeta', socialMeta)
  eleventyConfig.addShortcode('titleTag', titleTag)

  return
}
