/**
 * @file Defines the Accessiblity Statement page layout
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @since 1.0.0
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in JavaScript templates in Eleventy}
 */
export var data = {
  description: 'Copyright and licensing policies for all content and source code of my personal website–written in plain language.',
  shortTitle: 'Accessiblity',
  templateEngineOverride: '11ty.js,md',
  title: 'Accessibility Statement',
  weight: 3
}

/**
 * Defines markup for the Accessiblity Statement page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render({pkg: {repository}}) {
  return `<!--content/pages/policies/accessbility.11ty.js-->
I’m committed to making my site accessible for everyone, especially if you have a disability.

## Minimum standards

My site meets or exceeds [Web Content Accessibility Guidelines 2.1][wcag2aa] Level AA.

## Additional considerations

I also take the following measures to ensure my site remains accessible:

* Use clear written lanuage
* Make the user interface easy to understand
* Run automated and manual tests for accessibility
* Honor your system preferences for lighter or darker color schemes
* Limit file sizes and device memory loads
* Publish my source code in a [public repository][git], allowing anyone to inspect it and suggest changes
* [Document](/docs/) my code, to remain as transparent as possible—even if you have little or no programming experience

Reuben L. Lillie

[git]: ${repository.url}
[wcag2aa]: https://www.w3.org/WAI/standards-guidelines/wcag/`
}
