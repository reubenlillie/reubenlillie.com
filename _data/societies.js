/**
 * @file Contains global professional societies data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global societies data module
 * @module _data/societies
 * @since 1.0.0
 */
export default [
  {
    name: 'American Society of Composers, Authors, and Publishers ',
    website: 'https://www.ascap.com/'
  },
  {
    name: 'American Guild of Musical Artists',
    website: 'https://www.musicalartists.org/'
  },
  {
    name: 'National Association of Parliamentarians',
    website: 'https://www.parliamentarians.org/'
  },
  {
    name: 'Wesleyan Theological Society',
    website: 'https://wtsociety.com/'
  }
]
