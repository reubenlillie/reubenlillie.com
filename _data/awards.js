/**
 * @file Contains global author data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global author data module
 * @module _data/author
 * @since 1.0.0
 */
export default [
  {
    name: 'Robert Hale–Dean Wilder Voice Scholarship',
    organization: {
      name: 'Olivet Nazarene University School of Music',
      website: 'https://www.olivet.edu/school-music'
    },
    years: [2007, 2008, 2010]
  },
  {
    name: 'Michael J. Brick Memorial Scholarship',
    organization: {
      name: 'Roosevelt University Chicago College of Performing Arts',
      website: 'https://www.roosevelt.edu/colleges/ccpa'
    },
    years: {
      start: 2012,
      end: 2013
    }
  },
  {
    name: 'G. Ernest Wright Merit Scholarship',
    organization: {
      name: 'McCormick Theological Seminary',
      website: 'https://mccormick.edu/'
    },
    years: {
      start: 2013,
      end: 2016
    }
  },
  {
    name: 'Craighton T. and Linda G. Hippenhammer Faculty Scholarship',
    organization: {
      name: 'Olivet Nazarene University'
    },
    years: [2021]
  }
]
