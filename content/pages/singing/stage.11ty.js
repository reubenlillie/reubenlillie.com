/**
 * @file Defines the chained template for repertoire
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @since 1.0.0
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  description: 'Here’s a sample of my stage work.',
  shortTitle: 'Stage',
  tags: 'singing',
  templateEngineOverride: '11ty.js,md',
  title: 'Stage Repertoire',
  weight: 1
}

/**
 * Defines markup for the repertoire page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render({content, rep}) {
  return `<!--content/pages/singing/stage.11ty.js-->

I’m an operatic tenor, currently exploring lyric and lyrico-dramatic repertoire. I also perform musical theater and other contemporary commercial vocal techniques.

Here’s a sample of opera and musical theater roles I’m prepared to perform.

_Arranged alphabetically by composer_

${Object.values(rep.stage).map(composer => this.corpus(composer)).join('\n')}

<p class="small">From 2007 to 2019, I performed as a <a href="/baritone-rep/">baritone</a>.</p>`

}
