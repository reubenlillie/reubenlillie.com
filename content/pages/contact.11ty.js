/**
 * @file Defines the chained template for the contact page
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @since 1.0.0
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  description: 'Hi, I’m Reuben. I’m available for singing, speaking, writing, coding, teaching, and parliamentary engagements. Have voice, will travel.',
  form: true,
  layout: 'layouts/page',
  tags: 'headerNav',
  templateEngineOverride: '11ty.js,md',
  title: 'Contact Me',
  weight: 2
}

/**
 * Defines markup for the contact page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render(data) {
  /**
   * Destructure a portion of `data` 
   * @type {Object} 
   */
  var {author: {contact: {social}}} = data

  return  `<!--content/pages/contact.11ty.js-->

Thanks for reaching out!

You can send me a message with this form. Or feel free to use one of these social media platforms. I’ll get back to you soon.

<nav aria-label="social media nav">
  <ul class="flex flex-wrap gap no-list-style no-padding-inline-start space-between">
    ${social.map(account => this.socialLink(account)).join('\n')}
  </ul>
</nav>
<form id="contact"
  method="POST"
  action="/thanks/"
  autocomplete="on"
  netlify-honepot="bot_field"
  data-netlify="true"
  class="flex flex-column">
  <label class="screen-reader">Don’t fill this out if you’re human: <input name="bot_field"></label>
  <label for="name">Name</label>
  <input id="name"
   name="name"
   type="text" name="name"
   required="true">
  <label for="email">Email</label>
  <input id="email"
   name="email"
   type="email"
   required="true">
  <label for="message">Message</label>
  <textarea id="message"
    name="message"
    rows="13"
    cols="47"></textarea>
  <button id="submit"
    name="submit"
    type="submit">Send</button>
</form>`
}
