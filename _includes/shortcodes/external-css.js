/**
 * @file Defines a shortcode for loading external stylesheets
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Determine which CSS file to load based on environment variables
 * @param {string} fileSlug File slug for stylesheet in the `css/` directory
 * @return {string} Path to environment CSS
 */
function envCSS(fileSlug) {
  return process.env.ELEVENTY_ENV === 'production'
    ? `/css/${fileSlug}.min.css`
    : `/css/${fileSlug}.css`
}

/**
 * Defines markup for linking external stylesheets in the HTML `head`
 * @module _includes/shortcodes/external-css
 * @since 1.0.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.externalCSS(data)}`
 */
export default ({form, page: {fileSlug}, syntaxHighlighting}) => {
  /**
   * Placeholder for concatenating HTML 
   * @type {string}
   */
  var html = '<!--_includes/shortcodes/external-css-->'

  // Is the current page not the homepage? (The index page has no fileSlug.)
  fileSlug
    ? html += `<link href="${envCSS('base')}" rel="stylesheet" media="screen">`
  /* Use this line when there are styles for `./css/home.css`
    : html += `<link href="${envCSS('home')}" rel="stylesheet" media="all">`;
  */
    : html += '<!--add /css/home.css for specific homepage styling-->';
 
  // Is `form` set to `true` somewhere within Eleventy’s `data` cascade?
  if(form) {
    html += `<link href="${envCSS('forms')}" rel="stylesheet" media="screen">`
  }
  
  // Is `syntaxHighlighting` set to `true` in the `data` cascade?
  if(syntaxHighlighting) {
    html += `<link href="${envCSS('syntax-highlighting')}" rel="stylesheet" media="screen">`
  }

  // Load print styles last
  html += `<link href="${envCSS('print')}" rel="stylesheet" media="print">`

  // The concatenated markup
  return html
}
