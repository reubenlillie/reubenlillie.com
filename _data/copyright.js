/**
 * @file Contains global copyright data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global copyright data module
 * @module _data/copyright
 * @since 1.0.0
 */
export default {
  year: new Date().getFullYear(),
  holder: 'Reuben L. Lillie',
  license: {
    abbr: 'CC BY-SA',
    name: 'Creative Commons Attribution-ShareAlike 4.0 International license',
    url: 'https://creativecommons.org/licenses/by-sa/4.0/'
  }
}
