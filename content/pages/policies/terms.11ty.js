/**
 * @file Defines the Terms of Use page layout
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @since 1.0.0
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in JavaScript templates in Eleventy}
 */
export var data = {
  description: 'Website policies don’t have to be complicated. Mine are about as simple yet thorough as they come. Enjoy reading policy language for a change.',
  shortTitle: 'Terms',
  templateEngineOverride: '11ty.js,md',
  title: 'Terms of Use',
  weight: 2
}

/**
 * Defines markup for the Terms of Use page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render(data) {
  return `<!--content/pages/policies/terms.11ty.js-->

Thank you for visiting my site.

I want you to enjoy your experience on my site. And I want you to have peace of mind.

These terms of use apply my site ([https://reubenlillie.com/](/)).

## Accessibility

My [accessibility statement][a11y] explains the standards and processes I follow to ensure anyone can use my site, especially if you have a disability.

## Privacy and Copyright Protection

My [privacy policy][privacy] explains how I treat personal information and protect your privacy when you use my sites.

My [copyright notice][copyright] explains the permissions I give you when using content on my sites or the source code.

## Warranties

My site is provided “as is,” without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and noninfringement.

## Liability

I will not be liable for any loss or damage that is not reasonably foreseeable.

## About These Terms and Conditions

These terms define the relationship between you as a user and me as the owner and developer of my site. These terms do not create third party beneficiary rights.

I may update these terms or any additional terms that apply to my sites. If you do not agree to the modified terms, then you should discontinue your use of my site.

If there is a conflict between these terms and any additional terms, the additional terms will take precedence.

If you do not comply by these terms, and I do not take action right away, then that fact alone does not mean I am giving up any rights I may have (including the right to act in the future).

The laws of the State of Illinois, <abbr title="USA: United States of America">USA</abbr>, will apply to any disputes relating to these terms. All claims relating to these terms will be litigated exclusively in the federal or state courts of Cook County, Illinois, USA, and yours and my consent to personal jurisdiction in those courts.

Reuben L. Lillie

[a11y]: /accessiblity/
[copyright]: /copyright/
[privacy]: /privacy/`
}
