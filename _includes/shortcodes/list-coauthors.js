/**
 * @file Defines a filter for listing coauthors
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import fullName from '../filters/full-name.js'
import lastNameFirst from '../filters/last-name-first.js'

/**
 * Defines a list of two to four coauthors
 * @module _includes/filters/last-name-first
 * @since 1.0.0
 * @param {Object} coauthor From an array of coauthors
 * @param {number} index Position in the array of coauthors
 * @param {Object} arr The original array of coauthors
 * @return {string} HTML
 * @see Turabian 2018, 225–7, figure 18.1
 */
export default (coauthor, index, arr) => index === 0 
  ? `<!--first of coauthors in _includes/shortcodes/list-coauthors.js-->
${lastNameFirst(coauthor)}`
  : index !== arr.length - 1
    ? `<!--among coauthors in _includes/shortcodes/list-coauthors.js-->
${fullName(coauthor)}` 
    : `<!--last of coauthors in _includes/shortcodes/list-coauthors.js-->
and ${fullName(coauthor)}`
