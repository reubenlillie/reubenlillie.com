/**
 * @file Contains global metadata for configuring the site
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in 11ty}
 */

import author from './author.js'

/**
 * Global site data module.
 * @module _data/site
 */
export default {
  baseUrl: 'https://reubenlillie.com', // No trailing slash
  description: 'I’m a philosopher, musician, and theologian. I’m available for hire as a singer, speaker, author, web developer, instructor, and parliamentarian.',
  madeWith: 'Made 🏗 with 🫶🏻 from Chicago 🧄.',
  title: author.name.fullName,
  tagline: 'Philosopher, Musician, Theologian',
  locale : {
    code: 'en-US',
    dateOptions: {
      year: 'numeric', 
      month: 'long', 
      day: 'numeric'
    }
  }
}
