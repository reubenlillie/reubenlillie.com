/**
 * @file Defines the chained template for the singing bio
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 * @since 1.0.0
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in Eleventy}
 */
export var data = {
  description: 'Here’s a copy of my headshot and singer program bio.',
  image: {
    src: 'headshot.jpg',
    alt: 'Reuben’s headshot'
  },
  tags: 'activities',
  templateEngineOverride: '11ty.js,md',
  title: 'Singing',
  weight: 1
}

/**
 * Defines markup for my singer program bio
 * @param {Object} data 11ty’s `data` object
 * @return {String} HTML
 */
export async function render(data) {
  var {image} = data
  var headshot = await this.headshot({
    src: image.src, 
    alt: image.alt, 
    width: 300, 
    height: 300,
    classList: [
      'float',
      'float-left',
      'margin-inline-end',
      'margin-block-end'
    ]
  })

  return `<!--content/pages/singing/index.11ty.js-->
<div>${headshot}</div>

Known for his “virile sound” and “reliable technique,” _Opera News_ exclaims Reuben L. Lillie “graces his music with an intrinsically attractive timbre.” The Pennsylvania-born, Chicago-based tenor produces a sound that is both “vibrant and penetrating,” according to _Chicago Classical Review_ (_CCR_), “a solid vocal technique and ringing tenor voice.”

Reuben’s 2024 season included a return to Opera Festival of Chicago for Puccini’s _Manon_ (Edmondo), which _CCR_ praised for his “sweet lyric tenor” as well as for the Chicago premiere of Dallapicolla’s _Il prigioniero_ (Premiere sacerdote). Reuben also presented the tenor solos in Olivet Nazarene University’s _Messiah_: The Reunion concert alongside fellow alums Bradley Garvin, Cassandra Petrie, Jenna Fawcett, and Ashlie McIntire.

In November, Reuben debuted the role of the notorious Nathan Leopold in the world premiere of Felix Jarrar’s _HindSight_, coproduced by Chicago Fringe Opera and /kor/ productions and nominated for _Chicago Reader’s_ Best New Local Opera Production of 2024. _CCR_ called Reuben “a powerful vocal force and dramatic presence” and said that he was “particularly stirring in his final anguished cries of guilt.”

Other recent appearances include a concert performance of Mascagni’s _Cavalleria Rusticana_ (Turiddu) with West Suburban Symphony as well as Opera Festival of Chicago’s productions of Verdi’s _Attila_ (Uldino, covering Foresto), the Midwest premiere of Verdi’s _Il corsaro_ (Schiavo, covering Corrado), and the Chicago premiere of Pizzetti’s _Assassinio nella cattedrale_ (First Knight/Tempter) which the _Hyde Park Herald_ called his tenor “both ingratiating and menacing.”
`
}
