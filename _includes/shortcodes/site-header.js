/**
 * @file Defines a shortcode for displaying the page header
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import nav from './nav.js'

/**
 * Defines markup for the page `<header>`
 * @module _includes/shortcodes/site-header
 * @since 1.0.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.siteHeader(data)}`
 */
export default data => {
  /**
   * Destructure a portion of `data` 
   * @type {Object}
   */
  var {collections: {headerNav}, page} = data

  return `<!--_includes/shortcodes/site-header.js-->
<header id="site_header" class="x-large">
  <a class="screen-reader" href="#main">Skip to main content</a>
  ${nav(headerNav, page)}
</header>`
}
