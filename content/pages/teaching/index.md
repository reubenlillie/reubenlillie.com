---
title: Teaching
tags: activities
weight: 5
description: In case it weren’t glaringly obvious how much of a nerd I am, Ialso teach college students.
---

I currently serve as an adjunct professor for Olivet Nazarene University, where I’ve taught or guest lectured in five departments: music; theology and philosophy; the behavioral sciences; business; and computer science.

It’s a special privilege to teach for my alma mater, and I cherish working with the latest generation of students.

Because of my teaching assignments, I have a unique opportunity to work with students and faculty from several areas of study. So far, I’ve gotten into some academic mischief with folks in biology, chemistry, geology, computer science, business, art, music, theology, philosophy, legal studies, public policy, and sociology. A number of Olivet colleagues and I are also collaborating on several interdisciplinary research projects.

Check out some my [articles](/articles/), [papers](/papers/), and [presentations](/speaking/).

More research samples are available at [Olivet Digital Commons][digital-commons] and [Academia.edu](https://olivet.academia.edu/ReubenLillie), where you can also access my curriculum vitae.

[digital-commons]: https://digitalcommons.olivet.edu/do/search/?q=author_lname%3A%22Lillie%22%20AND%20author_fname%3A%22Reuben%22&start=0&context=1036391&sort=date_desc
