/**
 * @file Defines a shortcode for getting image URLs from the Cloudinary API
 * @author Reuben L. Lillie <rlillie@loopnaz.org>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#javascript-template-functions JavaScript template functions in Eleventy}
 */

import dotenv from 'dotenv'
import cloudinary from 'cloudinary'

dotenv.config()
cloudinary.v2.config({
  cloud_name: process.env.CLOUDINARY_NAME, 
  api_key: process.env.CLOUDINARY_API_KEY, 
  api_secret: process.env.CLOUDINARY_API_SECRET
})

/**
 * Requests an image URL from the Cloudinary API
 * @module _includes/shortcodes/from-cloudinary
 * @name authorMeta
 * @param {string} src Image source on Cloudinary
 * @return {string} URL
 * @see {@link https://cloudinary.com/documentation/ Cloudinary Documentation}
 * @example 
 * // In an Eleventy template
 * `${this.fromCloudinary(data)}`
 */
export default async (src, options) => {
  var defaults = {
    quality: 'auto',
    secure: true
  } 
  options = {...defaults, ...options}
  return await cloudinary.url(src, options)
}
