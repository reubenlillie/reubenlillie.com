/**
 * @file Defines a filter to minify HTML template files
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Import Juriy Zaytsev’s HTMLMinifier module
 * @see {@link https://github.com/kangax/html-minifier GitHub}
 */
import htmlmin from 'html-minifier'

/**
 * Optimize HTML template files in the output directory
 * @module _includes/transforms/minify-html
 * @since 1.0.0
 * @param {Object} eleventyConfig Eleventy’s Config API
 * @see {@link https://www.11ty.dev/docs/config/#transforms Transforms in Eleventy}
 */
export default (content, outputPath) => {
  if(outputPath.endsWith('.html') 
  && process.env.ELEVENTY_ENV === 'production') {
    var minified = htmlmin.minify(content, {
      useShortDoctype: true,
      removeComments: true,
      collapseWhitespace: true
    })
    return minified
  }
  return content
}
