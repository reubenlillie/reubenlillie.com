/**
 * @file Loads fuller versions of fonts loaded in the first stage
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @since 1.4.0
 * @see {@link https://www.zachleat.com/web/css-tricks-web-fonts/ “Developing a Robust Font Loading Strategy for CSS-Tricks” by Zach Leatherman}
 */

// Immediately invoked function expression
(function () {
  var directory = 'courier-prime'
  var family = 'Courier Prime'
  var regular = 'CourierPrime'

  // Client supports CSS Font Loading API
  if('fonts' in document) {
    /*
     * Create a new FontFace object
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/FontFace/FontFace FontFace.FontFace() Constructor on MDN}
     */
    var regularFace = new FontFace(family, `url(/fonts/${directory}/${regular}.woff2) format("woff2")`, {display: 'swap'})

    /*
     * Load font faces into the DOM
     */
    Promise.all([regularFace]).then(fonts =>
      fonts.forEach(font => document.fonts.add(font)))
  }

  /*
   * Fallback for browsers that don’t support the CSS Font Loading API by
   * injecting @font-face CSS at-rules
   * @see {@link https://caniuse.com/#feat=font-loading CSS Font Loading on “Can I Use…”}
   */
  if(!('fonts' in document) && 'head' in document) {
    var style = document.createElement('style')
    style.innerHTML = `@font-face {font-family: ${family}; src: url(/fonts/${directory}/${regular}.woff2) format("woff2");}`
    document.head.appendChild(style)
  }

  return
}())
