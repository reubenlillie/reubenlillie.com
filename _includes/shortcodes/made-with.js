/**
 * @file Defines a shortcode for displaying the “Made with love” message
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Defines markup for the “Made with love” message
 * @module _includes/shortcodes/made-with
 * @since 1.0.0
 * @param {string} message The “Made with love” message
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.madeWith(data.site.madeWith)}`
 */
export default message =>
`<!--_includes/shortcodes/made-with.js-->
<span>${message}</span>`
