/**
 * @file Defines the base template
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#function JavaScript functions as templates in Eleventy}
 */

/**
 * Defines markup for the base template, which wraps around other templates
 * @method
 * @since 1.0.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @see {@link https://www.11ty.dev/docs/shortcodes/ Shortcodes in Eleventy}
 * @see {@link https://www.11ty.dev/docs/layout-chaining/ Layout chaining in Eleventy}
 */
export async function render(data) {
  /**
   * Destructure a portion of `data`
   * @type {Object} 
   */
  var {page: {fileSlug}} = data

  return `<!--_includes/layouts/base.11ty.js-->
<!DOCTYPE html>
<html lang="en">
  ${await this.headTag(data)}
  <body class="grid margin ${fileSlug ? fileSlug : 'home'}">
    ${this.siteHeader(data)}
    <main id="main" class="flex flex-column">
      ${data.content}
    </main>
    ${this.siteFooter(data)}
    <script>${await this.minifyJS(this.fileToString('js/register-service-worker.js'))}</script>
  </body>
</html>`
}
