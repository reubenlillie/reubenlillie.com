/**
 * @file Contains global code syllabi data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global code syllabi data module
 * @module _data/syllabi
 * @since 1.0.0
 */
export default [
  {
    title: 'Ethical Web Development',
    courseNumber: 'COMP-496',
    institution: 'Olivet Nazarene University',
    school: 'Martin D. Walker School of Engineering',
    department: 'Department of Computer Science and Emerging Technologies',
    url: '/comp-496/'
  }
]
