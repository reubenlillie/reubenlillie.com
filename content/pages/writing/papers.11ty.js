/**
 * @file Defines the chained template for academic papers
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  description: 'Here’s a sample of conference papers and other research I’ve presented publically.',
  shortTitle: 'Papers',
  tags: 'writing',
  templateEngineOverride: '11ty.js,md',
  title: 'Academic Papers',
  weight: 2
}

/**
 * Defines markup for the papers listing page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render({papers}) {
  papers = this.sortBy(papers, 'date', true)
  return  `<!--content/pages/writing/papers.11ty.js-->

Here’s a sample of papers I’ve presented publicly.

Several of these projects are coauthored with [Charles L. Perabeau][perabeau], Dean of the School of Theology and Christian Ministry at Olivet Nazarene University.

You can find full text copies, abstacts, and other information on [PhilPapers][philPapers], at [Olivet Digital Commons][digital-commons], and [Academia.edu][academia].

_Arranged by date_

${papers.map(paper => this.authorDateReference(paper)).join('\n')}

[academia]: https://olivet.academia.edu/ReubenLillie
[digital-commons]: https://digitalcommons.olivet.edu/do/search/?q=author_lname%3A%22Lillie%22%20AND%20author_fname%3A%22Reuben%22&start=0&context=1036391&sort=date_desc
[perabeau]: https://www.olivet.edu/charles-perabeau
[philPapers]: https://philpeople.org/profiles/reuben-l-lillie`
}
