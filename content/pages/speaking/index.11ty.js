/**
 * @file Defines the chained template for presentations
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @since 1.0.0
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  description: 'I’m available for a variety of public speaking engagements: speeches, preaching, master classes, training sessions, you name it.',
  tags: 'activities',
  templateEngineOverride: '11ty.js,md',
  title: 'Speaking',
  weight: 3
}

/**
 * Defines the markup for the speaking page
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render({presentations}) {
  return `<!--content/pages/speaking/index.11ty.js-->

My formal disciplines are philosophy, theology, and music.

In addition to my work as a [singer](/singing/), I [teach](/teaching/) at Olivet Nazarene University and serve as a minister in the [Church of the Nazarene][church].

Since 2014, I’ve been advocating for [web ethics](/software/) in the open source community.

I’m also an expert in [parliamentary procedure](/parliamentary-procedure/), specializing in the not-for-profit sector. I believe in having meetings that help organizations live up to their ideals.

I’m available to speak on any of these topics. [Let’s keep the conversation going](/contact/).

Here’s a sample of presentations I’ve given at universities, conferences, houses of worship, and MeetUps.

_Arranged by date_

${presentations.map(presentation => 
  this.authorDateReference(presentation))
  .join('\n')}

[church]: https://loopnaz.org/`
}
