/**
 * @file Defines a filter to minify JavaScript inline
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/*
 * Import Terser module
 * @see {@link https://github.com/terser-js/terser GitHub}
 */
import {minify} from 'terser'

/**
 * Minify JavaScript
 * @module _includes/filters/minify-js
 * @since 1.0.0
 * @param {string} script A JavaScript file’s contents
 * @return {string} The minified script
 * See {@link https://www.11ty.dev/docs/quicktips/inline-js/ Eleventy Quicktip}
 * @see {@link https://www.11ty.dev/docs/data-js/#example-exposing-environment-variables Environment variables in Eleventy}
 * @example 
 * // In an Eleventy template
 * `${this.minifyJS($this.fileToString('/includes/assets/js/gratuitip.js'))}`
 */
export default async script => {
  // Only minify scripts for production
  if(process.env.ELEVENTY_ENV === 'production') {
    try {
      var minified = await minify(script);
      return minified.code
    } catch (err) {
      console.error('Terser error: ', err);
      return script
    }
  }

  return script
}
