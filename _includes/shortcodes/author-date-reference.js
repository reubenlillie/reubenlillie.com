/**
 * @file Defines a reference list item
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

// Filters
import fullName from '../filters/full-name.js'
import formatUrl from '../filters/format-url.js'
import formatTitle from '../filters/format-title.js'
import initialLetter from '../filters/initial-letter.js'
import isDate from '../filters/is-date.js'
import lastNameFirst from '../filters/last-name-first.js'

// Shortcodes
import listCoauthors from './list-coauthors.js'

/**
 * Defines a Chicago style author-date reference
 * @module _includes/shortcodes/author-date-reference
 * @since 1.0.0
 * @param {Object} entry
 * @return {string} HTML
 * @see Turabian 2018, chs. 18–19
 * @see {@link https://www.chicagomanualofstyle.org/turabian/turabian-author-date-citation-quick-guide.html Chicago Manual of Style}
 * @see {@link https://www.chicagomanualofstyle.org/turabian/Student-Tip-Sheets.html Turabian Tip Sheets}
 */
export default ({
  author,
  authors,
  coauthors,
  date,
  location,
  publication,
  presentedAt,
  sponsor,
  type,
  title,
  url
}) => {
  var entry
  var asAuthor = ''
  var year = ''

  title = formatTitle(title)

  if(date && type !== 'research') {
    year = isDate(date)
      ? `${date.getFullYear()}.`
      : typeof date === 'string' 
        ? `${date}.` 
        : 'n.d.'
  }

  // Single author
  if(author) {
    asAuthor = lastNameFirst(author)
  }

  // Multiple authors
  if(authors) {
    asAuthor = authors.length < 4
      ? authors.map((author, index, arr) => listCoauthors(author, index, arr)).join(', ')
      : `${authors} et al`
  }

  // Multiple authors, including me
  if(coauthors) {
    asAuthor = coauthors.length > 1
      ? `<!--I’m one of multiple authors-->
Coauthored with ${coauthors.map((author, index, arr) => listCoauthors(author, index, arr)).join(', ')}`
      : `<!--I’m one of two authors-->
Coauthored with ${fullName(coauthors[0])}`
  }

  /*
   * If there is a way to attribute and author,
   * then add a period to the end string.
   * Otherwise omit the author from citations.
   */
  asAuthor = asAuthor ? `${asAuthor}.` : ''

  /*
   * If there is a URL to attribute,
   * then wrap it in an anchor tag.
   * Otherwise omit the url from citations.
   */
  url = url ? `<a href="${url}" class="url">${formatUrl(url)}</a>.` : ''

  // TODO 19.1 Books

  // 19.2 Journal Articles
  if(type === 'journal') {
    journal = ''
    dateInfo = ''
    volume = ''
    issue = ''
    pages = ''
    if(publication) {
      /*
       * Destructure a portion of `publication` specific to journals
       * @type {Object}
       */
      var {name: journal, volume, issue, pages, dateInfo} = publication

      dateInfo = dateInfo
        ? ` (${dateInfo})`
        : ` (${date.toLocaleDateString('en', {month: 'long'})})`

      pages = pages ? `: ${pages}` : ''

      var journalTitle = `<em>${journal}</em>`

      var issueNumber = issue
        ? `${volume}, no. ${issue}`
        : volume

      var journalInfo = `${journalTitle} ${issueNumber}${dateInfo}${pages}`
    }

    title = `“${title}”`

    journalInfo = sponsor ? sponsor : journalInfo

    entry = `<!--${type}: see Turabian 2018, 19.2-->
${asAuthor} ${year} ${title} ${journalInfo}. ${url}`
  }

  // 19.3 Magazine Articles
  if(type === 'magazine') {
    /*
     * Destructure a portion of `publication` specific to magazines
     * @type {Object}
     */
    var {name: magazine} = publication

    date = date.toLocaleDateString('en', {month: 'long', day: 'numeric'})

    magazine = `<em>${magazine}</em>,`

    title = `“${title}”`

    entry = `<!--${type}: see Turbian 2018, 19.3-->
${asAuthor} ${year} ${title} ${magazine} ${date}. ${url}`
  }

  // 19.5 Websites, Blogs, and Social Media
  if(type === 'website') {
    /*
     * Destructure a portion of `publication` specific to websites
     * @type {Object}
     */
    var {page, owner} = publication

    asAuthor ? asAuthor : owner


    var website = asAuthor !== owner
      ? `${page}, ${owner}`
      : owner

    title = `“${title}”`

    entry = `<!--${type}: see Turabian 2018, 19.5-->
${asAuthor} ${year} ${title} ${website}. ${url}`
  }

  // TODO 19.6 Interviews and Personal Communications

  // TODO 19.7 Papers, Lectures, and Manuscript Collections
  if(type === 'lecture'
    || type === 'paper'
    || type === 'presentation'
    || type === 'sermon'
    || type === 'workshop'
    || type === 'master class') {
    dateInfo = dateInfo
      ? `${dateInfo}`
      : `${date.toLocaleDateString('en', {month: 'long', day: 'numeric'})}`

    sponsor = sponsor ? `${sponsor},` : ''

    title = `“${title}”`

    var asType = type === 'paper'
      ? `${initialLetter(type)} ${presentedAt ? presentedAt : 'presented at the'}`
      : `${initialLetter(type)},`
    var {name: place, city, state} = location
    var eventInfo = `<!--eventInfo-->${place ? `${place},` : ''} ${city ? `${city}` : ''}${state ? `, ${state}` : ''}${dateInfo ? `, ${dateInfo}` : ''}`
    entry = `<!--${type}: see Turabian 2018, 19.7.2-->
${asAuthor} ${year} ${title} ${asType} ${sponsor} ${eventInfo}. ${url}`
  }

  // TODO 19.8 Older Works and Sacred Works

  // TODO 19.9 Refernce Works and Secondary Citations

  // TODO 19.10 Soures in the Visual and Performing Arts

  // TODO 19.11 Public Documents

  return entry
    ? `<!--_includes/shortcodes/author-date-reference.js-->
<article class="hanging-indent">
  <p>${entry}</p>
</article>`
    : `<!--no author-date format for type: ${type}-->`
}
