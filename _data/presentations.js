/**
 * @file Contains global presentations data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global presentations data module
 * @module _data/presentations
 */
export default [
  {
    type: 'presentation',
    date: new Date(2021, 3, 15),
    title: 'Believing in Love: Intellectual Humility and Well-Being',
    sponsor: 'Craighton T. and Linda G. Hippenhammer Faculty Scholarship',
    location: {
      name: 'Olivet Nazarene University',
      city: 'Bourbonnais',
      state: 'IL'
    },
  },
  {
    type: 'presentation',
    date: new Date(2019, 7, 20),
    title: 'Leveraging the ‘J’ in Jamstack',
    sponsor: 'Chicago JavaScript MeetUp Group',
    location: {
      name: 'Cars.com',
      city: 'Chicago',
      state: 'IL'
    },
    url: 'https://www.meetup.com/js-chi/events/258154877/',
    cv: false
  },
  {
    type: 'presentation',
    date: new Date(2019, 6, 5),
    title: 'Templating without a Templating Language',
    sponsor: 'Jamstack Chicago MeetUp',
    location: {
      name: '8th Light, Inc.',
      city: 'Chicago',
      state: 'IL'
    },
    url: 'https://www.meetup.com/Jamstack-Chicago/events/261274989/',
    cv: false
  },
  {
    type: 'lecture',
    date: new Date(2019, 1, 20),
    title: 'Nonprofit Fundraising and Financial Reporting',
    sponsor: 'Jeff Williamson',
    location: {
      name: 'Olivet Nazarene University McGraw School of Business',
      city: 'Bourbonnais',
      state: 'IL'
    }
  },
  {
    type: 'sermon',
    date: new Date(2019, 2, 12),
    title: 'Receiving the Kingdom',
    sponsor: 'USA/Canada Festival of Young Nazarene Preachers at M19 Conference',
    location: {
      name: 'M19 Conference',
      city: 'Kansas City', 
      state: 'MO'
    },
    cv: false
  },
  {
    type: 'lecture',
    date: new Date(2018, 9, 22),
    title: 'Urban/Rural Sociology',
    sponsor: 'Charles L. Perabeau',
    location: {
      name: 'Olivet Nazarene University Department of Behavioral Sciences', 
      city: 'Bourbonnais', 
      state: 'IL'
    }
  },
  {
    type: 'workshop',
    date: new Date(2018, 10, 15),
    title: 'Parliamentary Procedure and Pizza',
    sponsor: 'Law and Politics Society',
    location: {
      name: 'Olivet Nazarene University',
      city: 'Bourbonnais', 
      state: 'IL'
    },
    cv: false
  },
  {
    type: 'lecture',
    date: new Date(2016, 2, 2),
    title: 'John Wesley 101: An Informal, No-Prior-Knowledge-Required Introduction to the Theologian and Founder of Methodism',
    sponsor: 'Disciples Divinity House 101 Series',
    location: {
      name: 'University of Chicago Divinity School', 
      city: 'Chicago', 
      state: 'IL'
    }
  },
  {
    type: 'master class',
    date: new Date(2015, 3, 7),
    title: 'Master class with Fourth Coast Ensemble',
    location: {
      name: 'Olivet Nazarene University School of Music', 
      city: 'Bourbonnais',
      state: 'IL'
    },
    cv: false
  }
]
