/**
 * @file Contains global papers data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global papers data module
 * @module _data/papers
 * @since 1.0.0
 */
export default [
  {
    type: 'paper',
    coauthors: [
      {
        first: 'Charles',
        middle: 'L',
        last: 'Perabeau'
      }
    ],
    date: new Date(2024, 2, 1),
    title: 'Empowering Colonized Communities: Integrating Districts in the Church of the Nazarene USA/Canada Region',
    sponsor: '59th annual meeting of the Wesleyan Theological Society',
    location: {
      name: 'Trevecca Nazarene University',
      city: 'Nashville',
      state: 'TN'
    }
  },
  {
    type: 'paper',
    date: new Date(2022, 3, 6),
    title: 'Believing in Love: Intellectual Humility and Well-Being',
    presentedAt: 'presented as part of the',
    sponsor: 'Craighton T. and Linda G. Hippenhammer Faculty Scholarship',
    location: {
      name: 'Olivet Nazarene University',
      city: 'Bourbonnais',
      state: 'IL'
    },
  },
  {
    type: 'paper',
    coauthors: [
      {
        first: 'Charles',
        middle: 'L',
        last: 'Perabeau'
      }
    ],
    date: new Date(2020, 2, 6),
    title: 'Dissident Districts: Developing a New Model for Administrative Boundaries in the Church of the Nazarene',
    sponsor: '55th annual meeting of the Wesleyan Theological Society',
    location: {
      name: 'Nazarene Theological Seminary',
      city: 'Kansas City',
      state: 'MO'
    }
  },
  {
    type: 'paper',
    date: new Date(2020, 2, 6),
    title: 'Confronting Hegemonic Homiletics: Returning Preaching to a Liturgical Act',
    sponsor: '55th annual meeting of the Wesleyan Theological Society',
    location: {
      name: 'Nazarene Theological Seminary',
      city: 'Kansas City',
      state: 'MO'
    },
    url: 'https://www.academia.edu/41644104/Confronting_Hegemonic_Homiletics_Returning_Preaching_to_a_Liturgical_Act'
  },
  {
    type: 'paper',
    date: new Date(2017, 2, 3),
    title: 'Wherever Two or Three Are Gathered: An Ecclesiology of Space',
    sponsor: '52nd annual meeting of the Wesleyan Theological Society',
    location: {
      name: 'Asbury Theological Seminary',
      city: 'Wilmore',
      state: 'KY'
    },
    url: 'https://www.academia.edu/35590768/Wherever_Two_or_Three_Are_Gathered_An_Ecclesiology_of_Space'
  },
  {
    type: 'paper',
    date: new Date(2017, 2, 2),
    coauthors: [
      {
        first: 'Charles',
        middle: 'L',
        last: 'Perabeau'
      }
    ],
    title: '#GeographyMatters: Keeping Religion and Race in Their Rightful Place',
    sponsor: '16th annual meeting of the Wesleyan Philosophical Society',
    location: {
      name: 'Asbury Theological Seminary',
      city: 'Wilmore',
      state: 'KY'
    },
    url: 'https://www.academia.edu/35593721/_GeographyMatters_Keeping_Religion_and_Race_in_Their_Rightful_Place'
  },
  {
    coauthors: [
      {
        first: 'Charles',
        middle: 'L',
        last: 'Perabeau'
      }
    ],
    date: new Date(2016, 2, 10),
    title: 'A World of Difference: Wesleyan Tradition and the Scientific Method in Global Contexts',
    type: 'paper',
    sponsor: '15th annual meeting of the Wesleyan Philosophical Society',
    location: {
      name: 'Point Loma Nazarene University',
      city: 'San Diego',
      state: 'CA'
    },
    url: 'https://www.academia.edu/35593675/A_World_of_Difference_Wesleyan_Tradition_and_the_Scientific_Method_in_Global_Contexts'
  },
  {
    type: 'paper',
    date: new Date(2015, 10, 5),
    title: 'Metaphysics and the New Birth: Regenerating Wesleyan Convictions',
    sponsor: 'Global Christianities Workshop',
    location: {
      name: 'University of Chicago Divinity School',
      city: 'Chicago',
      state: 'IL'
    },
    url: 'https://www.academia.edu/35593594/Metaphysics_and_the_New_Birth_Regenerating_Wesleyan_Convictions'
  },
  {
    type: 'paper',
    date: new Date(2015, 2, 5),
    title: 'Punctuation in Public Worship: The Semiotic Language Within Our Liturgies',
    sponsor: '14th annual meeting of the Wesleyan Philosophical Society',
    location: {
      name: 'Mount Vernon Nazarene University',
      city: 'Mount Vernon',
      state: 'OH'
    },
    url: 'https://www.academia.edu/35590134/Punctuation_in_Public_Worship_the_Semiotic_Language_within_Our_Liturgies'
  }
]  
