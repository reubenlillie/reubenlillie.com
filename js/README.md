# `./js/`

This directory contains client-side JavaScript files for the app. To load a script’s contents inline, use `<script>` tags combined with asynchronous calls to [`this.minifyJS()`](https://gitlab.com/reubenlillie/lectionary.app/-/blob/master/_includes/filters/minify-js.js) and [`this.fileToString()`](https://gitlab.com/reubenlillie/lectionary.app/-/blob/master/_includes/filters/file-to-string.js) inside a [JavaScript template](https://www.11ty.dev/docs/languages/javascript/)’s `render()` function.

For example:

```js
/**
 * The content of the template
 * @return {string} The rendered template
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 */
export async function render() { 
  return `<!-- Asynchronously load optimized versions of a client-side script -->
  <script>${await this.minifyJS(this.fileToString('js/offline.js'))}</script>`
```
