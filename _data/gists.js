/**
 * @file Contains global code gist data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global code gist data module
 * @module _data/gists
 * @since 1.0.0
 */
export default [
  {
    type: 'gist',
    name: 'Flexible, Full-Width, “Justified” Text Blocks',
    description: 'This layout is common in print, but it used to be a chore on the web. Read my <a href=\'https://codepen.io/reubenlillie/post/full-width-text-blocks\'>post on CodePen</a> about how to do it with CSS Flexbox, CSS Grid, and vanilla JavaScript.',
    url: 'https://codepen.io/reubenlillie/pen/xemGmd'
  }
]
