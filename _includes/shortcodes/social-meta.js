/**
 * @file Defines a universal shortcode for social media metadata
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/shortcodes/ 11ty shortcodes}
 */

/**
 * Defines OpenGraph and Twitter metadata with fallbacks
 * @module _includes/shortcodes/social-meta
 * @since 1.0.0
 * @param {Object} data 11ty’s `data` object
 * @return {string} HTML
 * @see {@link https://css-tricks.com/essential-meta-tags-social-media/ Adam Coti, “The Essential Meta Tags for Social Media,” _CSS-Tricks_ (updated December 21, 2016)}
 * @example 
 * // In an Eleventy template
 * `${this.socialMeta()}`
 */
export default ({
  description: pageDescription, 
  pkg: {url, ...pkg}, 
  site, 
  thumbnail: pageThumbnail,
  title: pageTitle
}) => {
  /**
   * Which title metadata to use 
   * @type {string}
   */
  var title = pageTitle ? pageTitle : site.title
  
  /**
   * Which desciption metadata to use 
   * @type {string}
   */
  var description = pageDescription ? pageDescription : pkg.description
  
  /**
   * Which thumbnail metadata to use 
   * @type {string}
   */
  var thumbnail = pageThumbnail ? pageThumbnail : 'favicons/android-chrome-512x512.png'
  
  return `<!--_includes/shortcodes/social-meta.js-->
<!--sharing-->
<meta property="og:title" content="${title}">
<meta name="twitter:title" content="${title}">
<meta property="og:description" content="${description}">
<meta property="twitter:description" content="${description}">
<meta property="og:image" content="${url}${thumbnail}">
<meta name="twitter:image" content="${url}${thumbnail}">
<meta name="twitter:card" content="summary">
<meta property="og:url" content="${url}">
<!--authentication-->
<link href="https://github.com/reubenlillie" rel="me">
<link href="https://zirk.us/@reubenlillie" rel="me">`
}
