/**
 * @file Defines a shortcode for displaying the page footer markup
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import copyrightNotice from './copyright-notice.js'
import madeWith from './made-with.js'
import nav from './nav.js'

/**
 * Defines markup for the page `<footer>`
 * @module _includes/shortcodes/site-footer
 * @since 1.0.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 * @example 
 * // In an Eleventy template
 * `${this.siteFooter(data)}`
 */
export default ({
  collections: {policies},
  copyright,
  page,
  site: {madeWith: message}
}) => `<!--_includes/shortcodes/site-footer.js-->
<footer id="site_footer" class="grid gap small">
  <p class="no-margin-block">${copyrightNotice(copyright)}&nbsp;${madeWith(message)}</p>
  ${nav(policies, page)}
</footer>`
