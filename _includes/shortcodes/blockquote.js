/**
 * @file Defines a shortcode for displaying block quotes
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Defines markup for a block quotation
 * @module _includes/shortcodes/blockquote
 * @since 1.0.0
 * @name blockquote
 * @param {Object} quote The quote to display
 * @return {string} A `<blockquote>`
 * @example 
 * // In an Eleventy template
 * `${this.blockquote(data)}`
 */
export default ({text, cite, src}) =>
`<!-- _includes/templates/blockquotes.js -->
<blockquote>
  <p>${text}</p>
  <cite>—${src ? `<a href="${src}">${cite}</a>` : cite}</cite>
</blockquote>`
