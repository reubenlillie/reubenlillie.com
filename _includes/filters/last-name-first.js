/**
 * @file Defines a filter for formatting a person’s name, last name first
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Displays a person’s last name, followed by first and middle (if any)
 * @module _includes/filters/last-name-first
 * @since 1.0.0
 * @param {Object} fullName A person’s full name
 * @return {string} The formatted name
 * @see Turabian 2018, 225–7, figure 18.1
 */
export default ({first, middle, last}) => {
  // Does the person use a middle name or initial?
  first = middle 
    ? `${first} ${middle}`
    : first

  return `${last}, ${first}`
}
