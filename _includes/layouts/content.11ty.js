/**
 * @file Defines the chained template for basic page content
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  layout: 'layouts/base'
}

/**
 * Defines markup for the main content
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render(data) {
  /**
   * Destructure a portion of `data`
   * @type {Object} 
   */
  var {
    collections, 
    content, 
    useContentTitle = true, 
    page,
    showDate,
    site: {baseUrl},
    tags, 
    title,
  } = data

  /**
   * The first or second tag in the `data.tags` array
   * @type {string}
   * @since 1.4.0 Check if the first tag is `pages` (directory data)
   * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and Directory Data Files in Eleventy}
   */
  var tag = tags
    // Is the first tag pages?
    ? tags[0] === 'pages'
      ? tags[1]
      : tags[0]
    : null

  var url = `${baseUrl}${page.url}`

  return `<!--_includes/layouts/content.11ty.js-->
${this.nav(collections.activities, page)}
${tag !== 'activities'
  ? `${this.nav(collections[tag], page)}`
  : `${this.nav(collections[page.fileSlug], page)}`
}
<article id="${page.fileSlug}_article">
  <header id="${page.fileSlug}_header">
    ${useContentTitle
      ? `<!--useContentTitle-->
  <h1>${title}</h1>`
      : `<!--useContentTitle: false in ${page.fileSlug}-->`
    }
    ${this.pdfLink(page.fileSlug)}
  </header>
  ${content}
  <footer id="main_footer" class="small">
    ${showDate ? `<p>Last updated ${this.pageDate(data)}</p>` : ''}
    <p class="show-on-screen-only">
      ${this.editThisPage(data)}
    </p>
    <p class="show-in-print-only">
      View this document online: <a href="${url}" class="url">${this.formatUrl(url)}</a>
    </p>
  </footer>
</article>`
}
