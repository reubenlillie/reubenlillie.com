/**
 * @file Defines the chained template for software projects
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 * @since 1.0.0
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  description: 'I taught myself how to code in 2014. Now I teach ethical web development at the college level. Let’s make something worthwhile together!',
  syntaxHighlighting: true,
  tags: 'writing',
  templateEngineOverride: '11ty.js,md',
  title: 'Software',
  weight: 3
}

/**
 * Defines the markup for the software template
 * @param {Object} data Eleventy’s `data` object
 * @return {string} HTML
 */
export function render({software}) {
  return `<!--content/pages/writing/software.11ty.js-->

I taught myself how to code in the summer of 2014. I was under an opera contract away from home. The rehearsal schedule was grueling. And I experienced some pretty impressive insomnia. I desperately needed an activity to help me wind down from singing—something quiet yet productive, still creative yet altogether different.

Now I specialize in web ethics: building fast, lightweight, accessible, secure sites and applications. I’m an outspoken advocate for vanilla JavaScript and Jamstack techniques. I helped found the [Jamstack Chicago MeetUp®](https://www.netlify.com/). And I’ve created an interdisciplinary course for the Martin D. Walker School of Engineering at Olivet Nazarene University called “[Ethical Web Development](/comp-496/).”

This site, for example, is built with the static site generator [Eleventy](https://11ty.dev/) and hosted by [Netlify](https://www.netlify.com/). Feel free to check out the [documentation pages](/docs/) and [source code](https://gitlab.com/reubenlillie/reubenlillie.com/).

[Let’s make something meaningful together](/contact/).
  
Here’s a sample of coding projects I’ve contributed to or created.

Check out more code on <a href="https://github.com/reubenlillie/">GitHub</a> and <a href="https://gitlab.com/reubenlillie/">GitLab</a>.

_Arranged by name_

${software.map(project => this.card(project)).join('')}`
}
