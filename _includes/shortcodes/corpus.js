/**
 * @file Defines a shortcode for displaying portions of a composer’s works
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Defines markup for a composer’s stage works
 * @param {Object} work Global data for a stage work
 * @return {string} HTML
 */
function stageWork({title, roles}) {
 return `<!--stageWork() in _includes/shortcodes/corpus.js-->
<li>
  <em>${title}</em>
  (${roles.map(role => `${role}`).join(', ')})
</li>`
}

/**
 * Defines markup for a composer’s stage works
 * @param {Object} work Global data for a concert work
 * @return {string} HTML
 */
function concertWork({largerWork, opusNumber, title}) {
  return `<!--concertWork() in _includes/shortcodes/corpus.js-->
<li>
  ${title.charAt(0) === '“'
    ? `${title}${largerWork ? ` from <em>${largerWork}</em>` : ''}${opusNumber ? `, ${opusNumber}` : ''}`
    : `<em>${title}</em>${opusNumber ? `, ${opusNumber}` : ''}`
  }
</li>`
}

/**
 * Defines markup for a composer’s body of work
 * @param {Object} composer Global data for a composer
 * @return {string} HTML
 */
export default ({name: composer, works}) => `<!--_includes/shortcodes/corpus.js-->
<article>
  <h3>${composer}</h3>
  <ul>
    ${works.map(work => 
      work.hasOwnProperty('roles')
        ? stageWork(work)
        : concertWork(work)
      ).join('\n')}
  </ul>
</article>`
