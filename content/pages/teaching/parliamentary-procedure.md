---
title: Parliamentary Procedure
tags: teaching
weight: 6
description: Don’t you wish you knew someone who knows Robert’s Rules of Order really well? Let’s talk &hellip;
---

I’m an expert in [_Robert’s Rules of Order_](https://robertsrules.com/) and a member of the [National Association of Parliamentarians][nap].

The best organizations have the best meetings. I believe in creating safe spaces where people feel free to share their greatest concerns and ideas. I also believe in getting work done as efficiently as possible.

I offer training for individuals and organizations. I’m also available for any of the following services:

* Professional presiding officer
* Convention parliamentarian
* Bylaws consultant
* Election supervisor
* Meeting strategist
* Script writer
* Expert witness

If you or your organization needs advice on parliamentary procedure, please [contact me](/contact/). I’m happy to help.

[nap]: https://parliamentarians.org/
