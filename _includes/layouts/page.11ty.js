/**
 * @file Defines the chained template for a basic page
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in Eleventy}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` method in Eleventy JavaScript templates}
 */
export var data = {
  layout: 'layouts/base'
}

/**
 * Defines markup for pages
 * @method
 * @name render()
 * @param {Object} data Eleventy’s `data` object
 * @return {String} HTML
 */
export function render(data) {
  /**
   * Destructure a portion of `data`
   * @type {Object} 
   */
  var {content, page: {fileSlug}, title} = data

  return `<!-- ./_includes/layouts/page.11ty.js -->
<article id="${fileSlug}_article">
  <header id="${fileSlug}_header">
    <h1>${title}</h1>
  </header>
  ${content}
</article>`
}
