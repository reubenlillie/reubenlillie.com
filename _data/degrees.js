/**
 * @file Contains global author data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

/**
 * Global author data module
 * @module _data/author
 * @since 1.0.0
 */
export default [
  {
    start: new Date(2006, 7, 23),
    end: new Date(2011, 4, 7),
    institution: {
      name: 'Olivet Nazarene University',
      address: {
        street: 'One University Avenue',
        city: 'Bourbonnais',
        state: {
          abbr: 'IL',
          name: 'Illinois'
        },
        zip: 60914
      },
      website: 'https://olivet.edu/'
    },
    degree: {
      abbr: 'BM',
      name: 'Bachelor of Music',
      major: 'Music Performance',
      concentration: 'Voice'
    },
  },
  {
    start: new Date(2011, 7, 24),
    end: new Date(2013, 4, 11),
    institution: {
      name: 'Roosevelt University Chicago College of Performing Arts',
      address: {
        street: '430 South Michicagn Avenue',
        city: 'Chicago',
        state: {
          abbr: 'IL',
          name: 'Illinois'
        },
        zip: 60605
      },
      website: 'https://www.roosevelt.edu/colleges/ccpa'
    },
    degree: {
      abbr: 'MM',
      name: 'Master of Music',
      major: 'Music Performance',
      concentration: 'Voice'
    }
  },
  {
    start: new Date(2013, 7, 26),
    end: new Date(2016, 4, 7),
    institution: {
      name: 'McCormick Theological Seminary',
      address: {
        street: '5460 South University Avenue',
        city: 'Chicago',
        state: {
          abbr: 'IL',
          name: 'Illinois'
        },
        zip: 60615
      },
      website: 'https://mccormick.edu/'
    },
    degree: {
      abbr: 'MDiv',
      name: 'Master of Divinity',
      major: 'Theology'
    }
  }
]
