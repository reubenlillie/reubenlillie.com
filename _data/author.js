/**
 * @file Contains global author data
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/data-global/ Global data files in Eleventy}
 */

import fullName from '../_includes/filters/full-name.js'

var name = {
  first: 'Reuben',
  middle: 'L',
  last: 'Lillie'
}

/**
 * Global author data module
 * @module _data/author
 * @since 1.0.0
 *        2.0.0 Add Bluesky 🦋; omit Mastodon 🐘 and LinkedIn 🟦
 */
export default {
  name: Object.assign({}, name, {fullName: fullName(name)}),
  contact: {
    address: {
      street: '633 S Plymouth Ct, Apt 703',
      city: 'Chicago',
      state: 'IL',
      zip: '60605'
    },
    email: 'reubenlillie@gmail.com',
    phone: {
      tel: '+13125158453',
      formatted: '+1 (312) 515-8453'
    },
    website: 'https://reubenlillie.com/',
    /**
     * Open Researcher and Contributor ID (ORCID iD)
     * A unique, persistent identifier free of charge to researchers
     * @see {@link https://orcid.org/  ORCID website}
     */
    orcid_id: '0000-0002-3229-5953',
    social: [
      {
        name: 'PhilPeople',
        user: 'reuben-l-lillie',
        url: 'https://philpeople.org/profiles/reuben-l-lillie',
        cv: true
      },
      {
        name: 'Academia',
        user: 'ReubenLillie',
        url: 'https://olivet.academia.edu/ReubenLillie/',
        cv: true
      },
      {
        name: 'Bluesky',
        user: '@reubenlillie.com',
        url: 'https://bsky.app/profile/reubenlillie.com',
        cv: true
      },
      {
        name: 'GitHub',
        user: '@reubenlillie',
        url: 'https://github.com/reubenlillie/',
        cv: true
      },
      {
        name: 'GitLab',
        user: '@reubenlillie',
        url: 'https://gitlab.com/reubenlillie/',
        cv: true
      },
      {
        name: 'Instagram',
        user: 'reuben.lillie',
        url: 'https://www.instagram.com/reuben.lillie/',
        cv: false
      }
    ]
  }
}
