/**
 * @file Contains global metadata for color codes
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * Global data module for colors
 * @module _data/colors
 * @since 1.0.0
 */
export default {
  black: {
    cmyk: '67% 33% 0% 99%',
    hex: '#010203',
    hsl: '210 50% 1%',
    rgb: '1 2 3'
  },
  darkGray: {
    cmyk: '1% 1% 0% 69%',
    hex: '#4f4f50',
    hsl: '240 1% 31%',
    rgb: '79 79 80'
  },
  lightGray: {
    cmyk: '0% 0% 0% 18%',
    hex: '#d0d0d0',
    hsl: '0 0% 82%',
    rgb: '208 208 208'
  },
  primary: {
    cmyk: '0% 43% 72% 19%',
    hex: '#ce7639',
    hsl: '25 60% 52%',
    rgb: '206 118 57'
  },
  white: {
    cmyk: '0% 0% 0% 0%',
    hex: '#fff',
    hsl: '0 0% 100%',
    rgb: '255 255 255'
  },
}
