---
description: Learn modern best practices for developing a more humane web by leveraging modern JavaScript, intrinsic design principles, and static site generation.
eleventyExcludeFromCollections: true
layout: layouts/content
numbering: true
tags: teaching
title: "COMP-496: Ethical Web Development"
---

<p class="flex justify-center no-margin"><em>Cybersecurity & Networking Seminar</em></p>
<blockquote>
  <p>It is difficult to get a man to understand something when his salary depends upon his not understanding it.</p>
  <p class="text-inline-end">—Upton Sinclair, <cite><em>Oakland Tribune</em></cite>, December 11, 1934</p>
</blockquote>
<blockquote cite="https://www.biography.com/activist/ida-b-wells">
  <p>The way to right wrongs is to turn the light of truth upon them.</p>
  <p class="text-inline-end">—Ida B. Wells</p>
</blockquote>
<dl>
  <dt>Institution</dt>
  <dd><a href="https://olivet.edu/">Olivet Nazarene University</a></dd>
  <dt>School</dt>
  <dd><a href="https://www.olivet.edu/walker-school-of-engineering">Martin D. Walker School of Engineering and Technology</a></dd>
  <dt>Department</dt>
  <dd><a href="https://www.olivet.edu/computer-science">Computer Science and Emerging Technologies</a></dd>
  <dt>Term</dt>
  <dd>Spring 2020</dd>
  <dt>Meeting Location</dt>
  <dd>Reed Hall of Science 212</dd>
  <dt>Meeting Time</dt>
  <dd>Tuesdays 5:30 p.m.–8:00 p.m.</dd>
  <dt>Credit Hours</dt>
  <dd>2</dd>
  <dt>Instructor</dt>
  <dd>Rev. Reuben L. Lillie, MM, MDiv</dd>
  <dt>Office</dt>
  <dd>Reed Hall of Science 324A</dd>
  <dt>Office Hours</dt>
  <dd>Tuesdays 10:00 a.m.–12:00 p.m. or by appointment</dd>
</dl>
<nav class="flex flex-column">
  <h2 id="contents" class="no-margin">Contents</h2>
  <ol>
    <li><a href="#course_description">Course Description</a></li>
    <li><a href="#course_rationale">Course Rationale</a></li>
    <li><a href="#course_aims_and_outcomes">Course Aims and Outcomes</a>
      <ol>
        <li><a href="#course_aims">Course Aims</a></li>
        <li><a href="#specific_learning_outcomes">Specific Learning Outcomes</a></li>
      </ol>
    </li>
    <li><a href="#format_and_procedures">Format and Procedures</a>
      <ol>
        <li><a href="#class_session_format">Class Session Format</a></li>
        <li><a href="#electronic_equipment_policy">Electronic Equipment Policy</a></li>
        <li><a href="#assignment_policy">Assignment Policy</a>
          <ol>
            <li><a href="#submission_process">Submission Process</a>
            <li><a href="#style_guidelines">Style Guidelines</a>
            <li><a href="#late_assignments">Late Assignments</a>
          </ol>
        </li>
      </ol>
    </li>
    <li><a href="#professors_assumptions">Professor’s Assumptions</a></li>
    <li><a href="#course_requirements">Course Requirements</a>
      <ol>
        <li><a href="#course_materials">Course Materials</a>
          <ol>
            <li><a href="#supplies">Supplies</a></li>
            <li><a href="#accounts_and_subscriptions">Accounts and Subscriptions</a></li>
            <li><a href="#other_required_and_supplemental_materials">Other Required and Supplemental Materials</a></li>
            <li><a href="#recommended_textbook">Recommended Textbook</a></li>
          </ol>
        </li>
        <li><a href="#attendance_policy">Attendance Policy</a></li>
        <li><a href="#participation_policy">Participation Policy</a></li>
        <li><a href="#vanilla_javascript_projects">Vanilla JavaScript Projects</a></li>
        <li><a href="#portfolio_project">Portfolio Project</a></li>
      </ol>
    </li>
    <li><a href="#grading_procedures">Grading Procedures</a></li>
    <li><a href="#statement_of_academic_integrity">Statement of Academic Integrity</a></li>
    <li><a href="#learning_support_services">Learning Support Services</a></li>
    <li><a href="#tentative_schedule">Tentative Schedule</a>
      <ol>
        <li><a href="#session_1">Session 1</a></li>
        <li><a href="#session_2">Session 2</a></li>
        <li><a href="#session_3">Session 3</a></li>
        <li><a href="#session_4">Session 4</a></li>
        <li><a href="#session_5">Session 5</a></li>
        <li><a href="#session_6">Session 6</a></li>
        <li><a href="#session_7">Session 7</a></li>
        <li><a href="#session_8">Session 8</a></li>
        <li><a href="#session_9">Session 9</a></li>
        <li><a href="#session_10">Session 10</a></li>
        <li><a href="#session_11">Session 11</a></li>
        <li><a href="#session_12">Session 12</a></li>
        <li><a href="#session_13">Session 13</a></li>
        <li><a href="#final_session">Final Session</a></li>
      </ol>
    </li>
    <li><a href="#notes">Notes</a></li>
    <li><a href="#references">References</a>
      <ol>
        <li><a href="#the_topic_of_ethics_in_software_and_web_development">The Topic of Ethics in Software and Web Development</a></li>
        <li><a href="#web_languages_and_methodologies">Web Languages and Methodologies</a>
          <ol>
            <li><a href="#a11y">Accessiblity (a11y)</a></li>
            <li><a href="#agile_software_development">Agile Software Development</a></li>
            <li><a href="#css">CSS (Cascading Styles Sheets)</a></li>
            <li><a href="#git">Git</a></li>
            <li><a href="#html">HTML (Hypertext Markup Language)</a></li>
            <li><a href="#http">HTTP (Hypertext Transfer Protocol)</a></li>
            <li><a href="#i18n_and_l10n">Internationalization (i18n) and Localization (L10n)</a></li>
            <li><a href="#jamstack_indieweb_and_static_site_generation">Jamstack, IndieWeb, and Static Site Generation</a></li>
            <li><a href="#javascript">JavaScript (ECMAScript)</a></li>
            <li><a href="#json">JSON (JavaScript Object Notation)</a></li>
            <li><a href="#progressive_web_apps">Progressive Web Apps</a></li>
            <li><a href="#svg">SVG (Scalable Vector Graphics)</a></li>
          </ol>
        </li>
      </ol>
    </li>
  </ol>
</nav>
<section>
  <h2 id="course_description">Course Description</h2>
  <p>This course introduces modern best practices for developing a more humane web. Students will explore the ethical ramifications behind various software decisions, including web performance, security, and user/developer experience. The course simulates a team-based working environment in which students will build individual portfolios exhibiting job-ready, front-end skills. In the process, students will learn to leverage modern JavaScript, CSS, Markdown, Git, intrinsic design principles, and static site generation. No prior coding experience necessary.</p>
</section>
<section>
  <h2 id="course_rationale">Course Rationale</h2>
  <p>In keeping with Olivet’s mission, this course provides a vital space for the University to explore “web development with a Christian purpose” together. Other courses in the curriculum are designed specifically for advanced computer science students who have met several prerequisites. By lowering the barrier to entry, however, this course invites students from a variety of fields to learn with and from one another. Furthermore, by focusing on the ethical ramifications behind various software decisions, students may better advocate for justice along their respective career paths.</p>
</section>
<section>
  <h2 id="course_aims_and_outcomes">Course Aims and Outcomes</h2>
  <h3 id="course_aims">Course Aims</h3>
  <p>The primary goal of this course is to explore the question, <em>How should we make the Web?</em> Too many who use the Internet know too little about how it works. Worse, many who do know how the Internet works take advantage of those who know less—and with little personal consequence. Profiteering, surveillance, and malicious intent abound online. As aspiring computer scientists, entrepreneurs, business leaders, content creators, and users of the Web in general, what can we do to ensure the otherwise worthwhile opportunities the Web provides do not simultaneously encourage the violation of users’ privacy, safety, or other rights?<p>
  <p>In addition to acquiring and improving upon basic coding skills, students will shape a personal philosophy of web development. With these skills and principles in hand, students will be better prepared to lead projects and companies in making the Web a more just space. None of us has any business learning to make the Web if we are not learning to make it better.</p>
  <h3 id="specific_learning_outcomes">Specific Learning Outcomes</h3>
  <p>By the end of this course, students will:
    <ul>
      <li>Write semantic, accessible, and performant HTML, CSS, and JavaScript</li>
      <li>Leverage distributed version control systems and static site generators</li>
      <li>Apply principles of Jamstack, mobile-first, and documentation-driven development</li>
      <li>Compare licensing practices for creative works</li>
      <li>Contribute to a class-led open source software project</li>
      <li>Build a personal online portfolio</li>
    </ul>
  </p>
</section>
<section>
  <h2 id="format_and_procedures">Format and Procedures</h2>
  <h3 id="class_session_format">Class Session Format</h3>
  <p>Each two-and-a-half-hour class session comprises five thirty-minute timetables: twenty-five minutes of work followed by a five-minute break. Each timetable is dedicated either to a brief lecture, discussion topic, tutorial, in-class project, or other task. By allowing for these periodic breaks, the instructor otherwise expects students’ full participation and undivided attention.</p>
  <p>The first half of the semester focuses on basic language training and design principles. After Spring Break, the semester shifts to a whole-class, team-based software project.</p>
  <h3 id="electronic_equipment_policy">Electronic Equipment Policy</h3>
  <p>Except in emergencies, students should silence and refrain from using mobile phones, personal computers, tablets, and other electronic devices without the professor’s invitation during class. While in session, workstations may only be used for material related to this course. In turn, the professor’s workstation can observe, gain access to, and project the screen contents for each student’s workstation. The professor reserves the right to enforce these guidelines at any time.</p>
  <h3 id="assignment_policy">Assignment Policy</h3>
  <h4 id="submission_process">Submission Process</h4>
  <p>Unless otherwise instructed, students will follow a two-step process for submitting code samples for grading.</p>
  <ol>
    <li>Push code sample assignments to the relevant GitLab project repository</li>
    <li>Post a link to the GitLab commit in the corresponding assignment in Canvas</li>
  </ol>
  <h4 id="style_guidelines">Style Guidelines</h4>
  <p>Each student is responsible for submitting grammatically correct work. All written assignments must follow the <cite>Chicago Manual of Style</cite> and <cite>Merriam-Webster’s Collegiate Dictionary</cite> as well as any templates and supplements distributed by the professor for style, formatting, grammar, mechanics, and spelling. In turn, all code samples and issue posts must follow the relevant coding standards and collaboration guidelines in the course GitLab project repository. Students are expected to use gender-inclusive, gender-neutral, and other inclusive forms of language whether speaking or writing. When in doubt, students should consult the professor.</p>
  <h4 id="late_assignments">Late Assignments</h4>
  <p>Any assignment a student fails to submit by the time it is due will automatically receive a grade reduction of fifty percentage points. Late assignments will be reduced an additional ten percent for each successive day that assignment is late. No assignment will be accepted after 4:00 p.m. on the final day of classes. Graduating seniors must likewise make arrangements to submit all work as instructed by the Office of the Registrar.</p>
</section>
<section>
  <h2 id="professors_assumptions">Professor’s Assumptions</h2>
  <p>While this course does not require any prior coding experience, the professor assumes each student, by registering for this course, (a) comes motivated to learn and improve upon all skills necessary throughout the semester; and (b) is comfortable making and troubleshooting mistakes in class. To that end, students should be prepared to allocate four to eight hours per week outside of class to complete readings, tutorials, and other assignments, as is typical of a two-credit-hour course. The work for a given session may not require that entire time allotment. The rapid pace and scope of this course, however, may require a student to complete additional research and tutorials outside of class. In any case, the professor aims to create a charitable and justifiably challenging learning environment for each student. Programming is hard! Students are welcome to work ahead. And students should not hesitate to ask the professor and other classmates for assistance.</p>
</section>
<section>
  <h2 id="course_requirements">Course Requirements</h2>
  <p>Students will:
    <ul>
      <li>Attend all class sessions</li>
      <li>Read, listen to, and view all written, audio, and video assignments</li>
      <li>Participate fully in class discussions</li>
      <li>Complete all assigned in- and out-of-class tutorials</li>
      <li>Complete all portfolio assignments</li>
    </ul>
  </p>
  <h3 id="course_materials">Course Materials</h3>
  <h4 id="supplies">Supplies</h4>
  <p>Students are expected to bring either (a) paper and a writing utensil or (b) a tablet and stylus to class.</p>
  <p>Although desktop and laptop computers are useful machines, they typically are not suitable for spatial reasoning and higher-order thinking, particularly the facilities needed in the planning phases of software development. Furthermore, the limited desktop space around classroom workstations is not conducive to other electronic equipment.</p>
  <h4 id="accounts_and_subscriptions">Accounts and Subscriptions</h4>
  <p>In lieu of a formal textbook, students are required to purchase the complete set of “Vanilla JavaScript Pocket Guides” by Chris Ferdinandi (<a href="https://vanillajsguides.com/complete-set/">https://vanillajsguides.com/complete-set/</a>). The professor has arranged for a special discount code with the author. Prior to purchase, students must contact the professor directly to receive the discount.</p>
  <p>Students must also maintain a personal GitLab account for completing assignments (<a href="https://gitlab.com/">https://gitlab.com/</a>). Those students who do not have a GitLab account will create one during the first class session.</p>
  <h4 id="other_required_and_supplemental_materials">Other Required and Supplemental Materials</h4>
  <p>All other required readings, audio files, or videos are free. Some publishers may require e-mail validation from the student before granting access.</p>
  <p>The professor will include external <abbr title="URL: Universal Resourse Locator">URL</abbr>s and other bibliographic information for all required and supplemental online materials in the references section of this syllabus or in Canvas modules for the relevant class session.</p>
  <h4 id="recommended_textbook">Recommended Textbooks</h4>
  <p>For those interested in more textbook-like resources, students are encouraged to purchase the following.</p>
  <div class="hanging-indent medium">
    <p>Falbe, Tina, Kim Anderson, and Martin Michael Fredricksen. <cite>The Ethical Design Handbook</cite>. Freiburg, Germany: Smashing Media, 2020.
    <p>Robbins, Jennifer N. <cite>Learning Web Development: A Beginner’s Guide to HTLM, CSS, JavaScript, and Web Graphics</cite>, 5th ed. Sebastopol, CA: O’Reilly, 2018.</p>
  </div>
  <h3 id="attendance_policy">Attendance Policy</h3>
  <p>Students should arrive promptly for each class session, sign in to a workstation, and be prepared to follow the professor’s instructions at the start of class.</p>
  <p>Bearing in mind that this course meets once per week and for a total of thirteen class sessions, the following guidelines for absences apply in accordance with the University’s Class Attendance Policy.<sup id="ref1"><a href="#n1">1</a></sup></p>
  <ul>
    <li>Each unexcused absence will automatically result in a loss of ten percentage points from the student’s final grade.</li>
    <li>At the professor’s discretion and in consultation with the department chair and Vice President of Academic Affairs, a student with three absences may be disqualified from receiving credit for the course.</li>
    <li>A student with more than three unexcused absences will definitely be disqualified from receiving credit.</li>
    <li>Excused absences are not penalized, but students must report them when they occur in order to be excused.</li>
    <li>Students receiving educational leniency for an absence will be marked as present.</li>
    <li>Students missing class for any reason remain personally responsible for submitting all assignments on time.</li>
  </ul>
  <h3 id="participation_policy">Participation Policy</h3>
  <p>Participation accounts for at least fifteen percent of the student’s final grade. Although students mostly will be seated at workstations during class, each student should be prepared, nonetheless, to respond appropriately to any and all questions and requests from the professor as well as any discussion prompts and other in-class assignments. The nature of the material further requires students to devise creative solutions to inexact problems. The professor, therefore, reserves the right to award or reduce any student’s final grade based on the quality of that student’s in-class participation throughout the term.</p>
  <h3 id="vanilla_javascript_projects">Vanilla JavaScript Projects</h3>
  <p>Students must complete all eight projects from Chris Ferdinandi’s “Vanilla JavaScript Pocket Guides.” Students also must complete a minimum of nine of the project ideas listed at <a href="https://learnvanillajs.com/projects/">https://learnvanillajs.com/projects/</a>.</p>
  <p>For extra credit, students may complete as many as eleven additional projects from the idea list (a maximum of two per week following Spring Break). Each extra credit project is eligible for up to five points (i.e., eleven projects at two points each equals a maximum of fifty-five points of extra credit, or 5.5% of the final grade). Note that the “Accordion” project idea is ineligible since it is already included among the Pocket Guides projects.</p>
  <h3 id="portfolio_project">Portfolio Project</h3>
  <p>Students will contribute to a personal portfolio project throughout the term. The professor will grade contributions individually as they are made. All other portions of the portfolio will be assessed at the end of the term.</p>
</section>
<section>
  <h2 id="grading_procedures">Grading Procedures</h2>
  <p>The professor will maintain a record of attendance and scholarship for each student. A report of the student’s class standing is given at the close of the semester. The following percentages are used to calculate final letter grades.</p>
  <table>
    <caption><strong>Final Grading Scale</strong></caption>
    <thead>
      <tr>
        <th scope="col">Percentage Range</th>
        <th scope="col">Letter Grade</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>93–100%</td>
        <td>A</td>
      </tr>
      <tr>
        <td>90–92%</td>
        <td>A-</td>
      </tr>
      <tr>
        <td>86–89%</td>
        <td>B+</td>
      </tr>
      <tr>
        <td>83–85%</td>
        <td>B</td>
      </tr>
      <tr>
        <td>80–82%</td>
        <td>B-</td>
      </tr>
      <tr>
        <td>76–79%</td>
        <td>C+</td>
      </tr>
      <tr>
        <td>73–75%</td>
        <td>C</td>
      </tr>
      <tr>
        <td>70–72%</td>
        <td>C-</td>
      </tr>
      <tr>
        <td>66–69%</td>
        <td>D+</td>
      </tr>
      <tr>
        <td>63–65%</td>
        <td>D</td>
      </tr>
      <tr>
        <td>60–62%</td>
        <td>D-</td>
      </tr>
      <tr>
        <td>0–59%</td>
        <td>F</td>
      </tr>
    </tbody>
  </table>
  <p>The professor will distribute a rubric for each assignment as relevant material is introduced. Assignments will be eligible for the following point values.</p>
  <table>
    <caption><strong>Point Distribution for Assessment</strong></caption>
    <thead>
      <tr>
        <th scope="col">Graded Item</th>
        <th scope="col">Point Value</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Hello World!</td>
        <td>30</td>
      </tr>
      <tr>
        <td>Vanilla JS Pocket Guide Projects (8 total)</td>
        <td>80</td>
      </tr>
      <tr>
        <td>Philosophy of Web Development—Rough Draft</td>
        <td>75</td>
      </tr>
      <tr>
        <td>Learn Vanilla JS Projects (9 minimum)</td>
        <td>90</td>
      </tr>
      <tr>
        <td>In-Class Project</td>
        <td>150</td>
      </tr>
      <tr>
        <td>Philosophy of Web Development—Final Draft</td>
        <td>75</td>
      </tr>
      <tr>
        <td>Portfolio Project</td>
        <td>350</td>
      </tr>
      <tr>
        <td>Participation</td>
        <td>150</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td><strong>Total</strong></td>
        <td><strong>1000</strong></td>
      </tr>
    </tfoot>
  </table>
</section>
<section>
  <h2 id="statement_of_academic_integrity">Statement of Academic Integrity</h2>
  <blockquote>
    <p>Seeking after truth is at the heart of an education at a Christian university like Olivet. ONU expects students to be truthful in all areas of life, including the academic arena. Those who engage in any form of academic dishonesty value their own gain more than their desire to seek truth; consequently, their behavior is incompatible with the goals and objectives of the University. Such dishonesty takes the form of cheating, plagiarism, or falsification. Specific examples include, but are not limited to:
    <ul>
        <li>Submitting another’s work as one’s own or allowing others to submit one’s work as though it were theirs.</li>
        <li>Failing to properly acknowledge authorities quoted, cited, or consulted in the preparation of oral or written work. All work submitted by students must represent their original work. Outside sources used as references should reveal the name and source and the extent to which the source is used.</li>
        <li>Tampering with experimental data to obtain a “desired” result or creating results for experiments not conducted.</li>
        <li>Lying or otherwise deceiving university personnel about academic matters.</li>
        <li>Falsifying college records, forms, or other documents.</li>
        <li>Students who knowingly assist another in dishonest behavior are held equally responsible.</li>
      </ul>
    <p class="align-right">—University <cite>Catalog</cite><sup id="ref2"><a href="#n2">2</a></sup></p>
  </blockquote>
</section>
</section>
<section>
  <h2 id="learning_support_services">Learning Support Services</h2>
  <blockquote>
    <p>Olivet Nazarene University welcomes students with disabilities and is committed to complying with laws regarding equal opportunity for students with disabilities. Qualified individuals who have a disability will not be excluded from participation in, denied the benefits of, or be subjected to discrimination in connection with the services, programs, or activities offered by ONU. These prohibitions against discrimination apply not only to the University’s academic programs but also to other programs and services at the University such as financial aid, housing, student employment, athletics, counseling and placement services, and extracurricular opportunities.</p>
    <p>The Office of Learning Support Services at Olivet Nazarene University is dedicated to supporting the University’s efforts toward accessibility and inclusion for students with disabilities. This is accomplished through collaboration, advocacy, education, and accommodation. The Office of Learning Support Services works with students on an individual, case-by-case basis determining appropriate, reasonable accommodations to promote the student’s learning, growth, development, and success. Those with questions should contact the office of Learning Support Services at <a href="mailto:lss@olivet.edu">lss@olivet.edu</a>.</p>
    <p class="align-right">—University <cite>Catalog</cite><sup id="ref3"><a href="#n3">3</a></sup></p>
  </blockquote>
</section>
<section>
  <h2 id="tentative_schedule">Tentative Schedule</h2>
  <h3 id="session_1">Session 1</h3>
  <ul>
    <li>Hypertext Markup Language (<abbr title="HTML: Hypertext Markup Language">HTML</abbr>) and Accessibility</li>
    <li>Cascading Style Sheets (<abbr title="CSS: Cascading Style Sheets">CSS</abbr>) Syntax, Cascade, and Box Model</li>
    <li>Free and Open Source Software (<abbr title="FOSS: Free and Open Source Software">FOSS</abbr>)</li>
    <li>Git: Command Line Interface (<abbr title="CLI: Command Line Interface">CLI</abbr>) and GitLab</li>
  </ul>
  <h3 id="session_2">Session 2</h3>
  <ul>
    <li>Git: Issues and Documentation-Driven Development</li>
    <li>CSS: Styling Text</li>
    <li>HTML: Multimedia and Hypertext Transfer Protocol (<abbr title="HTTP: Hypertext Transfer Protocol">HTTP</abbr>)</li>
    <li>CSS: Styling Images</li>
    <li>HTML: Tables and Data Lists</li>
  </ul>
  <h3 id="session_3">Session 3</h3>
  <ul>
    <li>HTML and CSS Documentation</li>
    <li>CSS: Layouts, Intrinsic Design, and Mobile-First Development</li>
    <li>CSS: Custom Properties and Object-Oriented CSS (<abbr title="OOCSS: Object-Oriented CSS">OOCSS</abbr>)</li>
    <li>HTML: Forms and Data Privacy</li>
    <li>Debugging HTML and CSS</li>
  </ul>
  <h3 id="session_4">Session 4</h3>
  <ul>
    <li>JavaScript: Document Object Model (<abbr title="DOM: Document Object Model">DOM</abbr>) Manipulation</li>
    <li>JavaScript: Strings and Numbers</li>
    <li>JavaScript: Arrays and Objects</li>
    <li>JavaScript: Variables, Functions, and Scope</li>
    <li>JavaScript: DOM Injection and Traversal</li>
  </ul>
  <h3 id="session_5">Session 5</h3>
  <ul>
    <li>Browser Storage and the General Data Protection Regulation (guest speaker): <a href="https://www.taftlaw.com/people/paul-d-mcgrady">Paul D. McGrady</a>, Taft Stettinius &amp; Hollister, LLP</li>
    <li>JavaScript: Documentation, Debugging, and Refactoring</li>
    <li>JavaScript: Application Programming Interfaces (<abbr title="API: Application Programming Interface">API</abbr>) and JavaScript Object Notation (<abbr title="JSON: JavaScript Object Notation">JSON</abbr>)</li>
    <li>Jamstack: Node.js and Eleventy Static Site Generator</li>
    <li>Jamstack: Markdown and Front Matter</li>
  </ul>
  <h3 id="session_6">Session 6</h3>
  <ul>
    <li>Modern Agile (guest speaker): <a href="https://www.industriallogic.com/people/tottinge">Tim Ottinger</a>, Industrial Logic</li>
    <li>JavaScript: Plugins and Code Reuse</li>
    <li>JavaScript: Web Apps and Working with Stateful Data</li>
    <li>Jamstack: Organizing the Code Base</li>
    <li>Transitioning to In-Class Project</li>
  </ul>
  <h3 id="session_7">Session 7</h3>
  <ul>
    <li>Cybersecurity (guest speaker): <a href="https://www.olivet.edu/jeffrey-rice">Prof. Jeffrey Rice</a>, Program Director for Cybersecurity</li>
    <li>Favicons</li>
    <li>Metadata</li>
  </ul>
  <h3 id="session_8">Session 8</h3>
  <ul>
    <li>Humane Documentation</li>
    <li>Web Typography</li>
  </ul>
  <h3 id="session_9">Session 9</h3>
  <ul>
    <li>Environment Variables</li>
    <li>Internationalization (<abbr title="i18n: Internationalization">i18n</abbr>) and Localization (<abbr title="L10n: Localization">L10n</abbr>)</li>
  </ul>
  <h3 id="session_10">Session 10</h3>
  <ul>
    <li>Scalable Vector Graphics (<abbr title="SVG: Scalable Vector Graphics">SVG</abbr>)</li>
  </ul>
  <h3 id="session_11">Session 11</h3>
  <ul>
    <li>Digging Deeper into CSS Grid</li>
  </ul>
  <h3 id="session_12">Session 12</h3>
  <ul>
    <li>Progressive Web Apps</li>
  </ul>
  <h3 id="session_13">Session 13</h3>
  <ul>
    <li>The Indie Web</li>
  </ul>
  <h3 id="final_session">Final Session</h3>
  <ul>
    <li>Portfolio Project Presentations</li>
  </ul>
</section>
<section>
  <h2 id="notes">Notes</h2>
  <ol class="no-padding-left list-inside indent medium">
    <li id="n1" class="margin-bottom"><a href="#ref1">^</a> Olivet Nazarene University, “Class Attendance Policy,” in <cite>Catalog</cite> 2019–2020, <a href="http://catalog.olivet.edu/content.php?catoid=7&navoid=408#Class_Attendance_Policy">http://catalog.olivet.edu/content.php?catoid=7&navoid=408#Class_Attendance_Policy</a>.</li>
    <li id="n2" class="margin-bottom"><a href="#ref2">^</a> Olivet, “Statement of Academic Integrity,” in <cite>Catalog</cite>, <a href="http://catalog.olivet.edu/content.php?catoid=7&navoid=408#academic-integrity">http://catalog.olivet.edu/content.php?catoid=7&navoid=408#academic-integrity</a>.</li>
    <li id="n3" class="margin-bottom"><a href="#ref3">^</a> Olivet, “Learning Support Services,” in <cite>Catalog</cite>, <a href="http://catalog.olivet.edu/content.php?catoid=7&navoid=408#learning_support_services">http://catalog.olivet.edu/content.php?catoid=7&navoid=408#learning_support_services</a>.</li>
  </ol>
</section>
<section class="hanging-indent medium">
  <h2 id="references">References</h2>
  <h3 id="the_topic_of_ethics_in_software_and_web_development">The Topic of Ethics in Software and Web Development</h3>
  <p>Burns, Heather. 2018. “How GDPR Will Change The Way You Develop.” <cite>Smashing Magazine</cite>, February 27. <a href="https://www.smashingmagazine.com/2018/02/gdpr-for-web-developers/">https://www.smashingmagazine.com/2018/02/gdpr-for-web-developers/</a>.</p>
  <p>Center for Humane Technology. n.d. “Center for Humane Technology: Realigning Technology with Humanity.” <a href="https://humanetech.com/">https://humanetech.com/</a>.</p>
  <p>Criado Perez, Caroline. 2019. <cite>Invisible Women: Data Bias in a World Designed for Men</cite>. New York: Abrams</p>
  <p>Eubanks, Virginia. 2018. <cite>Automating Inequality: How High-Tech Tools Profile, Police, and Punish the Poor</cite>. New York: St. Martins.</p>
  <p>Falbe, Tina, Kim Anderson, and Martin Michael Fredricksen. 2020. <cite>The Ethical Design Handbook</cite>. Freiburg, Germany: Smashing Media.</p>
  <p>Ferdinandi, Chris. 2019. <cite>The Lean Web</cite>. Self-published e-book. <a href="https://leanweb.dev/ebook/">https://leanweb.dev/ebook/</a>.</p>
  <p>Fry, Hannah. 2018. <cite>Hello World: Being Human in the Age of Algorithms</cite>. New York: W. W. Norton.</p>
  <p>Kadlec, Tim. 2019. “The Ethics of Web Performance.” January 9. <a href="https://timkadlec.com/remembers/2019-01-09-the-ethics-of-performance/">https://timkadlec.com/remembers/2019-01-09-the-ethics-of-performance/</a>.</p>
  <p>Keith, Jeremy. 2017. <cite>Resilient Web Design</cite>. Self-published e-book. <a href="https://resilientwebdesign.com/">https://resilientwebdesign.com/</a>.</p>
  <p>King, Kat. 2018. “Building Empathy-Driven Documentation.” Presentation at <cite>Write the Docs Portland <cite>2018</cite>, Crystal Ballroom, Portland, Oregon, May 7. Video, 28:01. <a href="https://www.youtube.com/watch?v=_HcmFvxxKaQ">https://www.youtube.com/watch?v=_HcmFvxxKaQ</a>.</p>
  <p>Linders, Ben. 2018. “Why Software Developers Should Take Ethics into Consideration.” <cite>InfoQ</cite>, March 8. <a href="https://www.infoq.com/news/2018/03/software-developers-ethics/">https://www.infoq.com/news/2018/03/software-developers-ethics/</a>.</p>
  <p>Noble, Safia Umoja and Brendesha M. Tynes, eds. 2016. <cite>The Intersectional Internet: Race, Sex, Class, and Culture Online</cite>. New York: Peter Lang.</p>
  <p>Noble, Safia Umoja. 2018. <cite>Algorithms of Oppression: How Search Engines Reinforce Racism</cite>. New York: New York University.</p>
  <p>Pomerantz, Jeffrey. 2015. <cite>Metadata</cite>. Cambridge, MA: MIT.</p>
  <p>Rand-Hendriksen, Morten. 2018. “Using Ethics In Web Design.” <cite>Smashing Magazine</cite>, March 22. <a href="https://www.smashingmagazine.com/2018/03/using-ethics-in-web-design/">https://www.smashingmagazine.com/2018/03/using-ethics-in-web-design/</a>.</p>
  <p>Scott, Adam. 2017. “Adam Scott on Ethical Web Development.” Interview by Jeff Bleiel. <cite>O’Reilly Programming Podcast</cite>. August 24. Audio, 19:52. <a href="https://www.oreilly.com/ideas/adam-scott-on-ethical-web-development/">https://www.oreilly.com/ideas/adam-scott-on-ethical-web-development/</a>.</p>
  <p>———. 2016–17. <cite>Ethical Web Development</cite>, four volumes. Sebastopol, CA: O’Reilly. <a href="https://ethicalweb.org/">https://ethicalweb.org/</a>.</p>
  <p>Simonite, Tom. 2018. “Should Data Scientists Adhere to a Hippocratic Oath?” <cite>Wired</cite>, February 8. <a href="https://www.wired.com/story/should-data-scientists-adhere-to-a-hippocratic-oath/">https://www.wired.com/story/should-data-scientists-adhere-to-a-hippocratic-oath/</a>.</p>
  <p>Waldman, Ari Ezra. 2018. <cite>Privacy as Trust: Information Privacy for an Information Age</cite>. New York: Cambridge.</p>
  <p>Vallor, Shannon. 2018. <cite>Technology and the Virtues: A Philosophical Guide to a Future Worth Wanting</cite>. New York: Oxford.</p>
  <p>Zuboff, Shoshana. 2019. <cite>The Age of Surveillance Capitalism: The Fight for a Human Future at the New Frontier of Power</cite>. New York: Hatchette.</p>
  <h3 id="web_languages_and_methodologies">Web Languages and Methodologies</h3>
  <p><em>Listed alphabetically</em></p>
  <h4 id="a11y">Accessibility (a11y)</h4>
  <p>Accessible Rich Internet Applications Working Group. 2019. “(<abbr title="WAI-ARAI: Web Accessibility Initiative-Accessible Rich Internet Applications">WAI-ARIA</abbr>) Authoring Practices 1.1.” World Wide Web Consortium (<abbr title="W3C: World Wide Web Consortium">W3C</abbr>). <a href="https://www.w3.org/TR/wai-aria-practices/">https://www.w3.org/TR/wai-aria-practices/</a>.</p>
  <p>The Accessibility Project. 2020. “The A11Y Project: A community-driven effort to make web accessibility easier.” <a href="https://a11yproject.com/">https://a11yproject.com/</a>.</p>
  <p>W3C. 2020. “Web Accessibility Initiative (<abbr title="Web Accessibility Initiative">WAI</abbr>).” <a href="https://www.w3.org/WAI/">https://www.w3.org/WAI/</a>.</p>
  <p>WebAIM. 2020. “WebAIM: Web Accessibility in Mind.” <a href="https://webaim.org/">https://webaim.org/</a>.</p>
  <h4 id="agile_software_development">Agile Software Development</h4>
  <p>Kerievsky, Joshua. 2016. “An Introduction to Modern Agile.” <cite>InfoQ</cite>, October 20. <a href="https://www.infoq.com/articles/modern-agile-intro/">https://www.infoq.com/articles/modern-agile-intro/</a>.</p>
  <p>“Modern Agile.” 2020. <a href="http://www.modernagile.org/">http://www.modernagile.org/</a>.</p>
  <p>Ottinger, Tim. 2018. “BONUS: Tim Ottinger on Practical Ways to Enable Psychological Safety: The Trust Transaction,” Interview by Vasco Duarte. <cite>Scrum Master Toolbox Podcast</cite>, May 5. Audio, 41:58. <a href=" https://scrum-master-toolbox.org/2018/05/podcast/bonus-tim-ottinger-on-practical-ways-to-enable-psychological-safety-the-trust-transaction/#more-3551">https://scrum-master-toolbox.org/2018/05/podcast/bonus-tim-ottinger-on-practical-ways-to-enable-psychological-safety-the-trust-transaction/#more-3551</a>.</p>
  <p>———. 2017. “Make People Awesome? Give Them Superpowers!” <cite>Agile Otter Blog</cite>, March 12. <a href="https://agileotter.blogspot.com/2017/03/make-people-awesome-give-them.html">https://agileotter.blogspot.com/2017/03/make-people-awesome-give-them.html</a>.</p>
  <h4 id="css">CSS (Cascading Style Sheets)</h4>
  <p>MDN. 2020. “CSS: Cascading Style Sheets.” <a href="https://developer.mozilla.org/en-US/docs/Web/CSS">https://developer.mozilla.org/en-US/docs/Web/CSS</a>.</p>
  <p>Leatherman, Zach. 2017. “A Comprehensive Guide to Font Loading Strategies.” <cite>Non-Canonical Web Standards Fan Fiction</cite>, August 14. <a href="https://www.zachleat.com/web/comprehensive-webfonts/">https://www.zachleat.com/web/comprehensive-webfonts/</a>.</p>
  <p>Simmons, Jen. 2019. “Layout Land.” YouTube channel. <a href="https://www.youtube.com/layoutland">https://www.youtube.com/layoutland</a>.</p>
  <p>Simmons, Jen, and Roger Black. 2017. “Web Typography & Layout: Past, Present, and Future.” Interview by Jeffrey Zeldman. <cite>A List Apart</cite>, December 6. Video, 1:13:10. <a href="https://alistapart.com/event/web-typography-layout-past-present-future/">https://alistapart.com/event/web-typography-layout-past-present-future/</a>.</p>
  <h4 id="git">Git</h4>
  <p>Chacon, Scott and Ben Straub. 2014. <cite>Pro Git</cite>, 2nd ed. New York: Apress. <a href="https://git-scm.com/book/en/v2">https://git-scm.com/book/en/v2</a>.</p>
  <p>Schafer, Corey. 2019. “Git Tutorials.” YouTube playlist, six videos, 2:24:44. <a href="https://www.youtube.com/playlist?list=PL-osiE80TeTuRUfjRe54Eea17-YfnOOAx">https://www.youtube.com/playlist?list=PL-osiE80TeTuRUfjRe54Eea17-YfnOOAx</a>.</p>
  <h4 id="html">HTML (Hypertext Markup Language)</h4>
  <p>Bernard, Philippe. 2019. “Favicons, Touch Icons, Tile Icons, etc. Which Do You Need?” <cite>CSS-Tricks</cite>, January 11. <a href="https://css-tricks.com/favicon-quiz/">https://css-tricks.com/favicon-quiz/</a>.</p>
  <p>Coti, Adam. 2016. “The Essential Meta Tags for Social Media.” <cite>CSS-Tricks</cite>, December 21. <a href="https://css-tricks.com/essential-meta-tags-social-media/">https://css-tricks.com/essential-meta-tags-social-media/</a>.</p>
  <p>Grundy, Julie. 2017. “The UX of Form Design: Designing an Effective Form.” Presentation at <cite>UX New Zealand</cite>, Shed 6, Wellington, New Zealand, November 1. <a href="http://www.uxnewzealand.com/2017-speakers/julie-grundy/">http://www.uxnewzealand.com/2017-speakers/julie-grundy/</a>.</p>
  <p>MDN. 2020. “HTML: Hypertext Markup Language.” <a href="https://developer.mozilla.org/en-US/docs/Web/HTML">https://developer.mozilla.org/en-US/docs/Web/HTML</a>.</p>
  <p>Web Hypertext Application Technology Working Group (WHATWG). 2020. “HTML Standard.” <a href="https://html.spec.whatwg.org/multipage/">https://html.spec.whatwg.org/multipage/</a>.</p>
  <h4 id="http">HTTP (Hypertext Transfer Protocol)</h4>
  <p>MDN. 2020. “HTTP.” <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP">https://developer.mozilla.org/en-US/docs/Web/HTTP</a>.</p>
  <p>W3C. 2014. “HTTP—Hypertext Transfer Protocol.” <a href="https://www.w3.org/Protocols/">https://www.w3.org/Protocols/</a>.</p>
  <h4 id="i18n_and_l10n">Internationalization (i18n) and Localization (L10n)</h4>
  <p>Coupé, Jérôme. 2019a. “Language Switcher for Multilingual JAMstack Sites.” <cite>Webstoemp</cite>, September 12. <a href="https://www.webstoemp.com/blog/language-switcher-multilingual-jamstack-sites/">https://www.webstoemp.com/blog/language-switcher-multilingual-jamstack-sites/</a>.</p>
  <p>———. 2019b. “Multilingual Sites with Eleventy.” <cite>Webstoemp</cite>, April 27. <a href="https://www.webstoemp.com/blog/multilingual-sites-eleventy/">https://www.webstoemp.com/blog/multilingual-sites-eleventy/</a>.</p>
  <p>MDN. 2020. “Creating Localizable Web Applications.” <a href="https://developer.mozilla.org/en-US/docs/Mozilla/Localization/Web_Localizability/Creating_localizable_web_applications">https://developer.mozilla.org/en-US/docs/Mozilla/Localization/Web_Localizability/Creating_localizable_web_applications</a>.</p>
  <p>W3C. 2018. “Internationalization.” <a href="https://www.w3.org/standards/webdesign/i18n">https://www.w3.org/standards/webdesign/i18n</a>.</p>
  <h4 id="jamstack_indieweb_and_static_site_generation">JAMstack, IndieWeb, and Static Site Generation</h4>
  <p>Biilmann, Mathias and Phil Hawksworth. 2019. <cite>Modern Web Development on the JAMstack</cite>. Sebastopol, CA: O’Reilly. <a href="https://www.netlify.com/oreilly-jamstack/">https://www.netlify.com/oreilly-jamstack/</a>.</p>
  <p>Coyier, Chris. 2020. “The Power of Serverless for Front-End Developers.” <a href="https://serverless.css-tricks.com/">https://serverless.css-tricks.com/</a>.</p>
  <p>IndieWeb Camp. 2020. “What is the IndieWeb?” <a href="https://indieweb.org/">https://indieweb.org/</a>.</p>
  <p>Leatherman, Zach. 2020. “Eleventy Documentation.” <a href="https://www.11ty.dev/docs/">https://www.11ty.dev/docs/</a>.</p>
  <p>———. 2019. “Own Your Content on Social Media Using the IndieWeb.” <cite>Non-Canonical Web Standards Fan Fiction</cite>, October 18. <a href="https://www.zachleat.com/web/own-your-content/">https://www.zachleat.com/web/own-your-content/</a>.</p>
  <p>Netlify. 2020. “Netlify Docs.” <a href="https://docs.netlify.com/">https://docs.netlify.com/</a>.</p>
  <p>———. 2020. “StaticGen: Top Open Source Static Site Generators.” <a href="https://www.staticgen.com/">https://www.staticgen.com/</a>.</p>
  <h4 id="javascript">JavaScript (ECMAScript)</h4>
  <p>Alicea, Anthony. 2015. “JavaScript: Understanding the Weird Parts.” <a href="http://learnwebdev.net/">http://learnwebdev.net/</a>.</p>
  <p>Crockford, Douglas. 2018. <cite>How JavaScript Works</cite>. San Jose, CA: Virgule-Solidus</p>
  <p>———. 2008. <cite>JavaScript: The Good Parts</cite>. Sebastopol, CA: O’Reilly.</p>
  <p>Ferdinandi, Chris. 2019a. “Vanilla JavaScript Pocket Guides.” <a href="https://vanillajsguides.com/">https://vanillajsguides.com/</a>.</p>
  <p>———. 2019b. “The Vanilla JS Toolkit.” <a href="https://vanillajstoolkit.com/">https://vanillajstoolkit.com/</a>.</p>
  <p>Haverbeke, Marijn. 2018. <cite>Eloquent JavaScript: A Modern Introduction to Programming</cite>, 3rd ed. San Francisco: No Starch. <a href="https://eloquentjavascript.net/">https://eloquentjavascript.net/</a>.</p>
  <p>JSDoc. 2017. “Use JSDoc.” <a href="https://jsdoc.app/">https://jsdoc.app/</a>.</p>
  <p>MDN. “JavaScript.” 2020. <a href="https://developer.mozilla.org/en-US/docs/Learn/JavaScript">https://developer.mozilla.org/en-US/docs/Learn/JavaScript</a>.</p>
  <p>Simpson, Kyle. 2019. <cite>You Don't Know JS Yet</cite>, 2nd ed., seven volumes. Frontend Masters. <a href="https://github.com/getify/You-Dont-Know-JS">https://github.com/getify/You-Dont-Know-JS</a>.</p>
  <h4 id="json">JSON (JavaScript Object Notation)</h4>
  <p>Crockford, Douglas. n.d. “JSON.” <a href="https://json.org/">https://json.org/</a>.</p>
  <p>ECMA International. 2017. “ECMA-404 The JSON Data Interchange Syntax,” 2nd ed. <a href="https://www.ecma-international.org/publications-and-standards/standards/ecma-404/">https://www.ecma-international.org/publications-and-standards/standards/ecma-404/</a>.</p>
  <p>GeoJSON. 2016. “The GeoJSON Format.” <a href="https://geojson.org/">https://geojson.org/</a>.</p>
  <p>Mapbox. n.d. “Tutorials.” <a href="https://docs.mapbox.com/help/tutorials/">https://docs.mapbox.com/help/tutorials/</a>.</p>
  <p>How To GraphQL. n.d. “The Fullstack Tutorial for GraphQL.” <a href="https://www.howtographql.com/">https://www.howtographql.com/</a>.</p>
  <h4 id="progressive_web_apps">Progressive Web Apps</h4>
  <p>Frain, Ben. 2019. “Designing And Building A Progressive Web Application Without A Framework,” three-part series. <cite>Smashing Magazine</cite>, July 23–30. <a href="https://www.smashingmagazine.com/2019/07/progressive-web-application-pwa-framework-part-3/">https://www.smashingmagazine.com/2019/07/progressive-web-application-pwa-framework-part-3/</a>.</p>
  <p>MDN. 2020. “Progressive Web Apps.” <a href="https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps">https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps</a>.</p>
  <p>Rzutkiewicz, Jason and Jeremy Lockhorn. 2019. “Why Progressive Web Apps Are The Future of Mobile Web: 2019 Research.” <cite>Y Media Labs</cite>. <a href="https://ymedialabs.com/progressive-web-apps">https://ymedialabs.com/progressive-web-apps</a>.</p>
  <h4 id="svg">SVG (Scalable Vector Graphics)</h4>
  <p>Coyier, Chris. 2019. “Using SVG.” <cite>CSS-Tricks</cite>, May 2. <a href="https://css-tricks.com/using-svg/">https://css-tricks.com/using-svg/</a>.</p>
  <p>Drasner, Sarah. 2019. “SVG Will Save Us.” Presenation at <cite>#PerfMatters Conference</cite>, Canada College Theater, Redwood City, CA, April 2. Video, 36:42. <a href="https://www.youtube.com/watch?v=sxte3WpyO60">https://www.youtube.com/watch?v=sxte3WpyO60</a>.</p>
  <p>W3C SVG Working Group. 2016. “Scalable Vector Graphics (SVG).” <a href="https://www.w3.org/Graphics/SVG/">https://www.w3.org/Graphics/SVG/</a>.</p>
</section>
