/**
 * @file Defines a shortcode for loading internal stylesheets
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

import fileToString from '../filters/file-to-string.js'
import minifyCSS from '../filters/minify-css.js'

/**
 * HTML `<style>` markup
 * @module _includes/shortcodes/internal-css
 * @since 0.5.0
 * @param {Object} data Eleventy’s `data` object
 * @return {string} The rendered shortcode
 * @see {@link https://www.11ty.dev/docs/data/ Using data in Eleventy}
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties Using CSS custom properties (variables) on MDN}
 * @example
 * // In an Eleventy template
 * `${this.internalCSS(data)}`
 */
export default ({
  colors: {black, darkGray, lightGray, primary, white}, 
  numbering
}) => {
  /**
   * CSS custom properties
   * @type {string}
   */
  var root = `/* _includes/shortcodes/internal-css.js */
    :root {
      /* Color Codes */
      ${white ? `--white: ${white.rgb};` : ''}
      ${lightGray ? `--lightGray: ${lightGray.rgb};` : ''}
      ${darkGray ? `--darkGray: ${darkGray.rgb};` : ''}
      ${black ? `--black: ${black.rgb};` : ''}
      ${primary ? `--primary: ${primary.rgb};` : ''}
      /* Named Properties */
      --background-color: rgb(var(--white));
      --text-color: rgb(var(--black));
      --background-gray: rgb(var(--lightGray));
      --text-gray: rgb(var(--darkGray));
      --border: 1px solid var(--background-gray);
    }
    @media screen {
      @media (prefers-color-scheme: dark) {
        :root {
          --background-color: rgb(var(--black));
          --text-color: rgb(var(--white));
          --background-gray: rgb(var(--darkGray));
          --text-gray: rgb(var(--lightGray));
        }
      }
    }`

  /**
   * File path for fonts stylesheet 
   * @type {string}
   */
  var fonts = 'css/fonts.css'

  /**
   * File path for layout stylesheet 
   * @type {string}
   */
  var layout = 'css/layout.css'

  /**
   * File path for numbering stylesheet 
   * @type {string}
   */
  var numberingCSS = 'css/numbering.css'

  /**
   * File path for typography stylesheet 
   * @type {string}
   */
  var typography = 'css/typography.css'

  /**
   * File path for colors stylesheet 
   * @type {string}
   */
  var colors = 'css/colors.css'

  /**
   * Placeholder for CSS to be added internally 
   * @type {string}
   */
  var css = ''

  // Order the file contents for internal CSS
  css += `/* ./${fonts} */ ${fileToString(fonts)}`
  css += `/* ./${layout} */ ${fileToString(layout)}`
  css += `/* ./${typography} */ ${fileToString(typography)}`
  css += `/* ./${colors} */ ${fileToString(colors)}`

  if(numbering) {
    css += `/* ./${numberingCSS} */ ${fileToString(numberingCSS)}`
  }

  return minifyCSS(root + css)
}
